-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema brunner
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `brunner` ;

-- -----------------------------------------------------
-- Schema brunner
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `brunner` DEFAULT CHARACTER SET utf8 ;
USE `brunner` ;

-- -----------------------------------------------------
-- Table `brunner`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `brunner`.`user` ;

CREATE TABLE IF NOT EXISTS `brunner`.`user` (
  `id_user` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `birth_date` DATE NULL,
  `gender` VARCHAR(20) NULL,
  `height` INT NULL,
  `weight` INT NULL,
  `login` VARCHAR(20) NOT NULL,
  `password` CHAR(60) NOT NULL,
  `role` ENUM('ROLE_USER', 'ROLE_ADMIN') NOT NULL DEFAULT 'ROLE_USER',
  `enabled` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_user`),
  UNIQUE INDEX `id_UNIQUE` (`id_user` ASC),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `brunner`.`training`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `brunner`.`training` ;

CREATE TABLE IF NOT EXISTS `brunner`.`training` (
  `id_training` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(20) NOT NULL,
  `date` DATE NOT NULL,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(500) NULL,
  `duration` VARCHAR(30) NOT NULL,
  `pace_avg` VARCHAR(30) NULL,
  `distance` INT NOT NULL,
  `distance_ascent` INT NULL,
  `distance_descent` INT NULL,
  `elevation_gained` INT NULL,
  `elevation_ascent` INT NULL,
  `elevation_descent` INT NULL,
  `elevation_min` INT NULL,
  `elevation_max` INT NULL,
  `heart_rate_avg` INT NULL,
  `heart_rate_max` INT NULL,
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id_training`),
  UNIQUE INDEX `id_UNIQUE` (`id_training` ASC),
  INDEX `fk_training_user_idx` (`id_user` ASC),
  CONSTRAINT `fk_training_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `brunner`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `brunner`.`segment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `brunner`.`segment` ;

CREATE TABLE IF NOT EXISTS `brunner`.`segment` (
  `id_segment` INT NOT NULL AUTO_INCREMENT,
  `segment_order` INT NOT NULL,
  `id_training` INT NOT NULL,
  PRIMARY KEY (`id_segment`),
  UNIQUE INDEX `id_UNIQUE` (`id_segment` ASC),
  INDEX `fk_segment_training1_idx` (`id_training` ASC),
  CONSTRAINT `fk_segment_training1`
    FOREIGN KEY (`id_training`)
    REFERENCES `brunner`.`training` (`id_training`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `brunner`.`point`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `brunner`.`point` ;

CREATE TABLE IF NOT EXISTS `brunner`.`point` (
  `id_point` INT NOT NULL AUTO_INCREMENT,
  `point_order` INT NOT NULL,
  `lattitude` FLOAT NOT NULL,
  `longtitude` FLOAT NOT NULL,
  `timestamp` VARCHAR(30) NOT NULL,
  `pace` VARCHAR(30) NULL,
  `elevation` FLOAT NULL,
  `heart_rate` INT NULL,
  `id_segment` INT NOT NULL,
  `id_training` INT NOT NULL,
  PRIMARY KEY (`id_point`),
  UNIQUE INDEX `id_UNIQUE` (`id_point` ASC),
  INDEX `fk_point_segment1_idx` (`id_segment` ASC),
  INDEX `fk_point_training1_idx` (`id_training` ASC),
  CONSTRAINT `fk_point_segment1`
    FOREIGN KEY (`id_segment`)
    REFERENCES `brunner`.`segment` (`id_segment`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_point_training1`
    FOREIGN KEY (`id_training`)
    REFERENCES `brunner`.`training` (`id_training`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `brunner`.`lap`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `brunner`.`lap` ;

CREATE TABLE IF NOT EXISTS `brunner`.`lap` (
  `id_lap` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(40) NOT NULL,
  `lap_order` INT NOT NULL,
  `complete` TINYINT(1) NOT NULL,
  `duration` VARCHAR(30) NOT NULL,
  `distance` FLOAT NOT NULL,
  `pace_avg` VARCHAR(30) NULL,
  `heart_rate_avg` INT NULL,
  `elevation_gained` INT NULL,
  `id_training` INT NOT NULL,
  PRIMARY KEY (`id_lap`),
  UNIQUE INDEX `id_UNIQUE` (`id_lap` ASC),
  INDEX `fk_lap_training1_idx` (`id_training` ASC),
  CONSTRAINT `fk_lap_training1`
    FOREIGN KEY (`id_training`)
    REFERENCES `brunner`.`training` (`id_training`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `brunner`.`record`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `brunner`.`record` ;

CREATE TABLE IF NOT EXISTS `brunner`.`record` (
  `id_record` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(40) NOT NULL,
  `duration` VARCHAR(30) NOT NULL,
  `distance` INT NOT NULL,
  `pace_avg` VARCHAR(30) NULL,
  `id_training` INT NOT NULL,
  `id_user` INT NOT NULL,
  PRIMARY KEY (`id_record`),
  UNIQUE INDEX `id_UNIQUE` (`id_record` ASC),
  INDEX `fk_record_training1_idx` (`id_training` ASC),
  INDEX `fk_record_user1_idx` (`id_user` ASC),
  CONSTRAINT `fk_record_training1`
    FOREIGN KEY (`id_training`)
    REFERENCES `brunner`.`training` (`id_training`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_record_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `brunner`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
