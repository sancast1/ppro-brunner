package cz.uhk.ppro.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Obecny interface pro DAO, z tohoto by mel kazdy dao interface dedit
 * @param <PK> Primary Key
 * @param <T> Model object
 */
public interface IAbstractDao<PK extends Serializable, T> {

    public T getByKey(PK key);

    public void persist(T entity);

    public void update(T entity);

    public void delete(T entity);

    public List<T> findAll();

}
