package cz.uhk.ppro.dao.impl;


import cz.uhk.ppro.dao.TrainingDao;
import cz.uhk.ppro.dto.Stats;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

@Repository("trainingDao")
public class TrainingDaoImpl extends AbstractDao<Integer, Training> implements TrainingDao {


    @Override
    public Training findById(int id) {return getByKey(id); }

    @Override
    @ModelAttribute("training")
    public void createTraining(Training training) { persist(training); }

    @Override
    public void deleteTraining(Training training) { delete(training); }

    @Override
    public void deleteTrainings(List<Training> trainings) {
        trainings.forEach(this::delete);
    }


    @SuppressWarnings("unchecked")
    @Override
    public List<Training> findAllTrainingsOfUser(User user) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("user", user ));

        List<Training> trainings = (List<Training>) criteria.list();

        return trainings;
    }




}
