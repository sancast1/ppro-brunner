package cz.uhk.ppro.dao.impl;

import cz.uhk.ppro.dao.RecordDao;
import cz.uhk.ppro.entity.Record;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("recordDao")
public class RecordDaoImpl  extends AbstractDao<Integer, Record> implements RecordDao {

    @Override
    public Record findById(int id) { return getByKey(id); }

    @Override
    public void createRecord(Record record) { persist(record); }

    @Override
    public void deleteRecord(Record record) { delete(record); }

    @Override
    public void deleteRecords(List<Record> records)  {
        records.forEach(this::delete);
    }

    @Override
    public List<Record> findAllRecordsOfTraining(Training training) {

        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("training", training ));

        List<Record> records = (List<Record>) criteria.list();

        return records;
    }

    @Override
    public List<Record> findAllRecordsOfUser(User user) {

        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("user", user ));
        //criteria.addOrder(Order.desc("distance"));

        return criteria.list();
    }

    @Override
    public List<Record> findAllRecordsWithTypeOfUser(User user, Record.Type type) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("user", user ));
        criteria.add(Restrictions.eq("type", type ));

        return criteria.list();
    }


    /*
    public List<Record> findAllRecordsOfUser(User user) {

        // criteria.addOrder(Order.asc(property));

        List<Record> records = new ArrayList<>();
        String[] distances = { "m400", "m800", "m1000", "m1500", "m3000", "m5000", "m10000", "Half_Marathon", "Marathon" };
        String[] durations = { "One_Hour", "Cooper_Test" };

        for (String distance : distances) {

            Criteria criteria = createEntityCriteria();
            criteria.add(Restrictions.eq("user", user ));
            criteria.add(Restrictions.eq("distance", distance ));
            criteria.addOrder(Order.desc("distance"));

            records.addAll(criteria.list());
        }

        for (String duration : durations) {
            Criteria criteria = createEntityCriteria();
            criteria.add(Restrictions.eq("user", user ));
            criteria.add(Restrictions.eq("duration", duration ));
            criteria.addOrder(Order.desc("duration"));

            records.addAll(criteria.list());
        }


        return records;
    }
    */




}
