package cz.uhk.ppro.dao;

import cz.uhk.ppro.entity.User;

import java.util.List;


public interface UserDao extends IAbstractDao<Integer, User> {

	User findUserByLogin(String login);

	User findById(int id);

	void createUser(User user);

	void deleteUser(User user);

	List<User> findAllUsers();


}
