package cz.uhk.ppro.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import cz.uhk.ppro.dao.IAbstractDao;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import cz.uhk.ppro.configuration.HibernateSessionContext;

/**
 * @param <PK> Primary Key
 * @param <T> Model object
 */
public abstract class AbstractDao<PK extends Serializable, T> implements IAbstractDao<PK, T> {

    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public AbstractDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
    	if (HibernateSessionContext.isOverridden()) {
    		return HibernateSessionContext.getSession();
    	}
        return sessionFactory.getCurrentSession();
    }

    /**
     * Returns entity by its primary key
     *
     * @param key primary key
     * @return entity
     */
    @SuppressWarnings("unchecked")
    @Override
    public T getByKey(PK key) {

        return (T) getSession().get(persistentClass, key);
    }

    /**
     * Saves new entity to database
     *
     * @param entity
     */
    @Override
    public void persist(T entity) {

        getSession().persist(entity);
    }

    /**
     * Updates entity in database
     *
     * @param entity
     */
    @Override
    public void update(T entity) {

        getSession().update(entity);
    }

    /**
     * Deletes entity from database
     *
     * @param entity
     */
    @Override
    public void delete(T entity) {

        getSession().delete(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll() {

        return createEntityCriteria().list();
    }


    protected Criteria createEntityCriteria() {

        return getSession().createCriteria(persistentClass);
    }

}
