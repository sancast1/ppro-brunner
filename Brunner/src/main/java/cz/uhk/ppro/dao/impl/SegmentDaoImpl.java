package cz.uhk.ppro.dao.impl;

import cz.uhk.ppro.dao.SegmentDao;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

@Repository("segmentDao")
public class SegmentDaoImpl  extends AbstractDao<Integer, Segment> implements SegmentDao {


    @Override
    public Segment findById(int id) {return getByKey(id); }


    @Override
    @ModelAttribute("segment")
    public void createSegment(Segment segment) { persist(segment); }


    @Override
    public void deleteSegment(Segment segment) { delete(segment); }


    @Override
    public void deleteSegments(List<Segment> segments) {
        segments.forEach(this::delete);
    }

    @Override
    public List<Segment> getAllSegmentsOfTraining(Training training) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("training", training ));

        List<Segment> segments = (List<Segment>) criteria.list();

        return segments;
    }
}
