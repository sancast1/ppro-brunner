package cz.uhk.ppro.dao.impl;

import cz.uhk.ppro.dao.PointDao;
import cz.uhk.ppro.entity.Point;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("pointDao")
public class PointDaoImpl  extends AbstractDao<Integer, Point> implements PointDao {

    @Override
    public Point findById(int id) { return getByKey(id); }

    @Override
    public void createPoint(Point point) { persist(point); }

    @Override
    public void deletePoint(Point point) { delete(point); }


    @Override
    public void deletePoints(List<Point> points) {
        points.forEach(this::delete);
    }

    @Override
    public List<Point> findAllPointsOfSegment(Segment segment) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("segment", segment ));

        List<Point> points = (List<Point>) criteria.list();

        return points;
    }

    @Override
    public List<Point> findAllPointsOfTraining(Training training) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("training", training ));

        List<Point> points = (List<Point>) criteria.list();

        return points;
    }


}
