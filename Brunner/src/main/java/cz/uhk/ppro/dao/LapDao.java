package cz.uhk.ppro.dao;


import cz.uhk.ppro.entity.Lap;
import cz.uhk.ppro.entity.Training;

import java.util.List;

public interface LapDao extends IAbstractDao<Integer, Lap>{

    Lap findById(int id);

    void createLap(Lap lap);

    void deleteLap(Lap lap);

    void deleteLaps(List<Lap> laps);

    List<Lap> findAllLapsOfTraining(Training training);

    List<Lap> findAllLapsOfTrainingOfType(Training training, Lap.Type type);

}
