package cz.uhk.ppro.dao.impl;

import cz.uhk.ppro.dao.LapDao;
import cz.uhk.ppro.entity.Lap;
import cz.uhk.ppro.entity.Training;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("lapDao")
public class LapDaoImpl  extends AbstractDao<Integer, Lap> implements LapDao {

    @Override
    public Lap findById(int id) { return getByKey(id); }

    @Override
    public void createLap(Lap lap) { persist(lap); }

    @Override
    public void deleteLap(Lap lap) { delete(lap); }

    @Override
    public void deleteLaps(List<Lap> laps)  {
        laps.forEach(this::delete);
    }

    @Override
    public List<Lap> findAllLapsOfTraining(Training training) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("training", training ));

        List<Lap> laps = (List<Lap>) criteria.list();

        return laps;
    }

    @Override
    public List<Lap> findAllLapsOfTrainingOfType(Training training, Lap.Type type) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("training", training ));
        criteria.add(Restrictions.eq("type", type ));

        List<Lap> laps = (List<Lap>) criteria.list();

        return laps;
    }


}
