package cz.uhk.ppro.dao;

import cz.uhk.ppro.entity.Record;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;

import java.util.List;
import java.util.Set;


public interface RecordDao extends IAbstractDao<Integer, Record>{

    Record findById(int id);

    void createRecord(Record record);

    void deleteRecord(Record record);

    void deleteRecords(List<Record> records);

    List<Record> findAllRecordsOfTraining(Training training);

    List<Record> findAllRecordsOfUser(User user);

    List<Record> findAllRecordsWithTypeOfUser(User user,Record.Type type);

}
