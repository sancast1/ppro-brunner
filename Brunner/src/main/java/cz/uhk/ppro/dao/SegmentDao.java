package cz.uhk.ppro.dao;

import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;

import java.util.List;


public interface SegmentDao extends IAbstractDao<Integer, Segment>{

    Segment findById(int id);

    void createSegment(Segment segment);

    void deleteSegment(Segment segment);

    void deleteSegments(List<Segment> segments);

    List<Segment> getAllSegmentsOfTraining(Training training);

}
