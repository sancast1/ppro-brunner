package cz.uhk.ppro.dao.impl;

import cz.uhk.ppro.configuration.Constants;
import cz.uhk.ppro.dao.UserDao;

import cz.uhk.ppro.entity.User;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Repository;


@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {


	@Override
	public User findUserByLogin(String login) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("login", login));
        return (User) criteria.uniqueResult();
	}


    @Override
    public User findById(int id) {

        return getByKey(id);
    }


    @Override
    public void createUser(User user) {
        String pw_hash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(Constants.BCRYPT_STRENGTH));

        Query query = getSession().createSQLQuery(
                "INSERT INTO `user` (`first_name` ,`last_name`, `birth_date`, `gender`, `height`, `weight`, `login`, `password`, `role`, `enabled`) "+
                "VALUES (:first_name, :last_name, :birth_date, :gender, :height, :weight, :login, :pw_hash, :role, :enabled);");

        query.setString("first_name", user.getFirstName());
        query.setString("last_name", user.getLastName());
        query.setDate("birth_date", user.getBirthDate());
        query.setString("gender", user.getGender().name());
        query.setInteger("height", user.getHeight());
        query.setInteger("weight", user.getWeight());
        query.setString("login", user.getLogin());
        query.setString("pw_hash", pw_hash);
        query.setString("role", user.getRole().name());
        query.setBoolean("enabled", user.isEnabled());

        query.executeUpdate();
    }


    @Override
    public void deleteUser(User user) {

        delete(user);
    }


    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAllUsers() {
        Criteria criteria = createEntityCriteria();
        List<User> users = (List<User>) criteria.list();
        return users;
    }




}
