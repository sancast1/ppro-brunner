package cz.uhk.ppro.dao;

import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;

import java.util.List;


public interface TrainingDao extends IAbstractDao<Integer, Training>{

    Training findById(int id);

    void createTraining(Training training);

    void deleteTraining(Training training);

    void deleteTrainings(List<Training> trainings);

    List<Training> findAllTrainingsOfUser(User user);

}
