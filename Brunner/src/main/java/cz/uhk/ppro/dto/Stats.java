package cz.uhk.ppro.dto;

import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.utils.FormatUtils;

import java.time.Duration;


public class Stats {

    private int distance;
    private int distanceGPS;
    private int distanceManual;
    private String duration;
    private String pace;
    private int distanceMetersAscent;
    private int distanceMetersDescent;
    private int elevationGained;
    private int elevationAscent;
    private int elevationDescent;
    private int elevationMin;
    private int elevationMax;
    private int heartRateMax;

    private int numberOfTrainings;
    private int distanceAvg;
    private String durationAvg;

    private Training longestTrainingByDistance;
    private Training longestTrainingByDuration;
    private Training fastestPaceTraining;


    public Stats(int distance, Duration duration, Duration pace,  int distanceMetersAscent, int distanceMetersDescent, int elevationGained, int elevationAscent, int elevationDescent, int elevationMin, int elevationMax, int heartRateMax, int numberOfTrainings, int distanceAvg, Duration durationAvg, Training longestTrainingByDistance, Training longestTrainingByDuration, Training fastestPaceTraining, int distanceGPS, int distaceManual) {
        this.distance = distance;
        this.duration = FormatUtils.convertDurationToStringHHmmss(duration);
        this.pace = FormatUtils.convertDurationToStringHHmmss(pace);
        this.distanceMetersAscent = distanceMetersAscent;
        this.distanceMetersDescent = distanceMetersDescent;
        this.elevationGained = elevationGained;
        this.elevationAscent = elevationAscent;
        this.elevationDescent = elevationDescent;
        this.elevationMin = elevationMin;
        this.elevationMax = elevationMax;
        this.heartRateMax = heartRateMax;
        this.numberOfTrainings = numberOfTrainings;
        this.distanceAvg = distanceAvg;
        this.durationAvg = FormatUtils.convertDurationToStringHHmmss(durationAvg);
        this.longestTrainingByDistance = longestTrainingByDistance;
        this.longestTrainingByDuration = longestTrainingByDuration;
        this.fastestPaceTraining = fastestPaceTraining;
        this.distanceGPS = distanceGPS;
        this.distanceManual = distaceManual;
    }

    public Stats(){}


    @Override
    public String toString() {
        return "Stats{" +
                "distance=" + distance +
                ", duration='" + duration + '\'' +
                ", pace='" + pace + '\'' +
                ", distanceMetersAscent=" + distanceMetersAscent +
                ", distanceMetersDescent=" + distanceMetersDescent +
                ", elevationGained=" + elevationGained +
                ", elevationAscent=" + elevationAscent +
                ", elevationDescent=" + elevationDescent +
                ", elevationMin=" + elevationMin +
                ", elevationMax=" + elevationMax +
                ", heartRateMax=" + heartRateMax +
                ", numberOfTrainings=" + numberOfTrainings +
                ", distanceAvg=" + distanceAvg +
                ", durationAvg='" + durationAvg + '\'' +
                ", longestTrainingByDistanceId=" + longestTrainingByDistance +
                ", longestTrainingByDurationId=" + longestTrainingByDuration +
                ", fastestPaceTrainingId=" + fastestPaceTraining +
                '}';
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPace() {
        return pace;
    }

    public void setPace(String pace) {
        this.pace = pace;
    }

    public int getDistanceMetersAscent() {
        return distanceMetersAscent;
    }

    public void setDistanceMetersAscent(int distanceMetersAscent) {
        this.distanceMetersAscent = distanceMetersAscent;
    }

    public int getDistanceMetersDescent() {
        return distanceMetersDescent;
    }

    public void setDistanceMetersDescent(int distanceMetersDescent) {
        this.distanceMetersDescent = distanceMetersDescent;
    }

    public int getElevationGained() {
        return elevationGained;
    }

    public void setElevationGained(int elevationGained) {
        this.elevationGained = elevationGained;
    }

    public int getElevationAscent() {
        return elevationAscent;
    }

    public void setElevationAscent(int elevationAscent) {
        this.elevationAscent = elevationAscent;
    }

    public int getElevationDescent() {
        return elevationDescent;
    }

    public void setElevationDescent(int elevationDescent) {
        this.elevationDescent = elevationDescent;
    }

    public int getElevationMin() {
        return elevationMin;
    }

    public void setElevationMin(int elevationMin) {
        this.elevationMin = elevationMin;
    }

    public int getElevationMax() {
        return elevationMax;
    }

    public void setElevationMax(int elevationMax) {
        this.elevationMax = elevationMax;
    }

    public int getHeartRateMax() {
        return heartRateMax;
    }

    public void setHeartRateMax(int heartRateMax) {
        this.heartRateMax = heartRateMax;
    }

    public int getNumberOfTrainings() {
        return numberOfTrainings;
    }

    public void setNumberOfTrainings(int numberOfTrainings) {
        this.numberOfTrainings = numberOfTrainings;
    }

    public int getDistanceAvg() {
        return distanceAvg;
    }

    public void setDistanceAvg(int distanceAvg) {
        this.distanceAvg = distanceAvg;
    }

    public String getDurationAvg() {
        return durationAvg;
    }

    public void setDurationAvg(String durationAvg) {
        this.durationAvg = durationAvg;
    }

    public int getDistanceGPS() {
        return distanceGPS;
    }

    public void setDistanceGPS(int distanceGPS) {
        this.distanceGPS = distanceGPS;
    }

    public int getDistanceManual() {
        return distanceManual;
    }

    public void setDistanceManual(int distanceManual) {
        this.distanceManual = distanceManual;
    }


    public Training getLongestTrainingByDistance() {
        return longestTrainingByDistance;
    }

    public void setLongestTrainingByDistance(Training longestTrainingByDistance) {
        this.longestTrainingByDistance = longestTrainingByDistance;
    }

    public Training getLongestTrainingByDuration() {
        return longestTrainingByDuration;
    }

    public void setLongestTrainingByDuration(Training longestTrainingByDuration) {
        this.longestTrainingByDuration = longestTrainingByDuration;
    }

    public Training getFastestPaceTraining() {
        return fastestPaceTraining;
    }

    public void setFastestPaceTraining(Training fastestPaceTraining) {
        this.fastestPaceTraining = fastestPaceTraining;
    }
}
