package cz.uhk.ppro.dto;


import cz.uhk.ppro.entity.Record;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;
import cz.uhk.ppro.utils.FormatUtils;

public class dtoRecord {

    private Record.Type type;
    private String duration;
    private String pace;
    private int distance;
    private Training training;
    private User user;

    public dtoRecord(Record record){

        this.type = record.getType();
        this.duration = FormatUtils.convertDurationToStringHHmmss(record.getDuration());
        this.pace = FormatUtils.convertDurationToStringMMss(record.getPace());
        this.distance = (int) record.getDistance();

        this.training = record.getTraining();
        this.user = record.getUser();
    }


    public Record.Type getType() {
        return type;
    }

    public void setType(Record.Type type) {
        this.type = type;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPace() {
        return pace;
    }

    public void setPace(String pace) {
        this.pace = pace;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
