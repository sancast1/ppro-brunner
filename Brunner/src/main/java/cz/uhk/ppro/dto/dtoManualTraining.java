package cz.uhk.ppro.dto;


import cz.uhk.ppro.entity.Training;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.Duration;
import java.util.Date;


public class dtoManualTraining implements Serializable {

    private int id;
    private int userId;
    @DateTimeFormat( pattern="dd.MM.yyyy" )
    private Date date;
    private int durationHours;
    private int durationMinutes;
    private int durationSeconds;
    private Duration duration;
    private int distanceMeters;
    private Training.Type type = Training.Type.MANUAL;

    private String name;
    private String description;
    private int distanceMetersAscent;
    private int distanceMetersDescent;
    private int elevationGained;
    private int elevationAscent;
    private int elevationDescent;
    private int elevationMin;
    private int elevationMax;
    private int heartRateAvg;
    private int heartRateMax;


    public dtoManualTraining(){
        userId = 0;
        date = new Date();
        duration = Duration.ZERO;
        name = "";
        description = "";
        elevationGained = 0;
        elevationAscent = 0;
        elevationDescent = 0;
        elevationMin = 0;
        elevationMax = 0;
        heartRateAvg = 0;
        heartRateMax = 0;
        distanceMeters = 0;
        distanceMetersAscent = 0;
        distanceMetersDescent = 0;
    }


    public dtoManualTraining(Training training){
        this();

        this.userId = training.getUser().getId();
        this.id = training.getId();
        this.date = training.getDate();
        this.duration = training.getDuration();
        this.name = training.getName();
        this.description = training.getDescription();
        this.elevationGained = training.getElevationGained();
        this.elevationAscent = training.getElevationAscent();
        this.elevationDescent = training.getElevationDescent();
        this.elevationMin = training.getElevationMin();
        this.elevationMax = training.getElevationMax();
        this.heartRateAvg = training.getHeartRateAvg();
        this.heartRateMax = training.getHeartRateMax();
        this.distanceMeters = training.getDistanceMeters();
        this.distanceMetersAscent = training.getDistanceMetersAscent();
        this.distanceMetersDescent = training.getDistanceMetersDescent();

        this.durationHours = (int) duration.toHours();
        this.durationMinutes = (int) duration.minusHours(durationHours).toMinutes();
        this.durationSeconds = (int) duration.minusHours(durationHours).minusMinutes(durationMinutes).getSeconds();

    }


    public Duration getDuration() {
        return Duration.ofHours(durationHours).plusMinutes(durationMinutes).plusSeconds(durationSeconds);
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(int durationHours) {
        this.durationHours = durationHours;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public int getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(int durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public int getDistanceMeters() {
        return distanceMeters;
    }

    public void setDistanceMeters(int distanceMeters) {
        this.distanceMeters = distanceMeters;
    }

    public Training.Type getType() {
        return type;
    }

    public void setType(Training.Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDistanceMetersAscent() {
        return distanceMetersAscent;
    }

    public void setDistanceMetersAscent(int distanceMetersAscent) {
        this.distanceMetersAscent = distanceMetersAscent;
    }

    public int getDistanceMetersDescent() {
        return distanceMetersDescent;
    }

    public void setDistanceMetersDescent(int distanceMetersDescent) {
        this.distanceMetersDescent = distanceMetersDescent;
    }

    public int getElevationGained() {
        return elevationGained;
    }

    public void setElevationGained(int elevationGained) {
        this.elevationGained = elevationGained;
    }

    public int getElevationAscent() {
        return elevationAscent;
    }

    public void setElevationAscent(int elevationAscent) {
        this.elevationAscent = elevationAscent;
    }

    public int getElevationDescent() {
        return elevationDescent;
    }

    public void setElevationDescent(int elevationDescent) {
        this.elevationDescent = elevationDescent;
    }

    public int getElevationMin() {
        return elevationMin;
    }

    public void setElevationMin(int elevationMin) {
        this.elevationMin = elevationMin;
    }

    public int getElevationMax() {
        return elevationMax;
    }

    public void setElevationMax(int elevationMax) {
        this.elevationMax = elevationMax;
    }

    public int getHeartRateAvg() {
        return heartRateAvg;
    }

    public void setHeartRateAvg(int heartRateAvg) {
        this.heartRateAvg = heartRateAvg;
    }

    public int getHeartRateMax() {
        return heartRateMax;
    }

    public void setHeartRateMax(int heartRateMax) {
        this.heartRateMax = heartRateMax;
    }
}
