package cz.uhk.ppro.dto;


import cz.uhk.ppro.entity.Lap;
import cz.uhk.ppro.utils.FormatUtils;

import javax.persistence.Transient;
import java.time.Duration;

public class dtoLap {

    private int order;
    private float distanceMeters;
    private boolean complete;
    private Duration duration;
    private Duration pace;

    public dtoLap(){}

    public dtoLap(Lap lap){
        this.order = lap.getOrder();
        this.complete = lap.isComplete();
        this.duration = lap.getDuration();
        this.pace = lap.getPace();
        this.distanceMeters = lap.getDistanceMeters();
    }

    @Transient
    public String getDurationString() {
        return FormatUtils.convertDurationToStringHHmmss(duration);
    }
    @Transient
    public String getPaceString() {
        return FormatUtils.convertDurationToStringMMss(pace);
    }


    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public float getDistanceMeters() {
        return distanceMeters;
    }

    public void setDistanceMeters(float distanceMeters) {
        this.distanceMeters = distanceMeters;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public Duration getPace() {
        return pace;
    }

    public void setPace(Duration pace) {
        this.pace = pace;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public int getPaceHours(){
        return (int) pace.toHours();
    }

    public int getPaceMinutes(){
        return (int) pace.minusHours(getPaceHours()).toMinutes();
    }

    public int getPaceSeconds(){
        return (int) pace.minusHours(getPaceHours()).minusMinutes(getPaceMinutes()).getSeconds();
    }

    @Override
    public String toString() {
        return "Lap: " + order + " distance: " + distanceMeters + " time: " + FormatUtils.convertDurationToStringHHmmss(duration) + " complete? " + complete;
    }


}
