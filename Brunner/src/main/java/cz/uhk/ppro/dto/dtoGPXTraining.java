package cz.uhk.ppro.dto;

import cz.uhk.ppro.entity.Training;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

public class dtoGPXTraining {

	private MultipartFile file;
	private String name;
	private String description;
	@DateTimeFormat( pattern="dd.MM.yyyy" )
	private Date date;
	private int userId;
	private int id;
	private int heartRateAvg;
	private int heartRateMax;
	Training.Type type = Training.Type.GPS;


	public dtoGPXTraining(){
		name = "";
		description = "";
		date =new Date();
		userId = 0;
		id = 0;
		heartRateAvg = 0;
		heartRateMax = 0;
	}

	public dtoGPXTraining(Training training) {
		this.id = training.getId();
		this.name = training.getName();
		this.description = training.getDescription();
		this.date =  training.getDate();
		this.userId = training.getUser().getId();
		this.heartRateMax = training.getHeartRateMax();
		this.heartRateAvg = training.getHeartRateAvg();

	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getHeartRateAvg() {
		return heartRateAvg;
	}

	public void setHeartRateAvg(int heartRateAvg) {
		this.heartRateAvg = heartRateAvg;
	}

	public int getHeartRateMax() {
		return heartRateMax;
	}

	public void setHeartRateMax(int heartRateMax) {
		this.heartRateMax = heartRateMax;
	}
}
