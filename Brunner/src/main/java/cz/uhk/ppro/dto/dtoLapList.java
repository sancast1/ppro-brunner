package cz.uhk.ppro.dto;


import cz.uhk.ppro.entity.Lap;

import java.util.List;

public class dtoLapList {
    private List<dtoLap> laps;
    private Lap.Type type;


    public dtoLapList(){}

    public dtoLapList(List<dtoLap> laps, Lap.Type type) {
        this.laps = laps;
        this.type = type;
    }


    public List<dtoLap> getLaps() {
        return laps;
    }

    public void setLaps(List<dtoLap> laps) {
        this.laps = laps;
    }

    public Lap.Type getType() {
        return type;
    }

    public void setType(Lap.Type type) {
        this.type = type;
    }
}
