package cz.uhk.ppro.dto;


import cz.uhk.ppro.entity.Point;

public class dtoPoint {
    private int order;
    private float lattitude;
    private float longtitude;


    public dtoPoint(){}

    public dtoPoint(Point point){
        this.order = point.getOrder();
        this.lattitude =  point.getLattitude();
        this.longtitude = point.getLongtitude();
    }

    public float getLattitude() {
        return lattitude;
    }

    public void setLattitude(float lattitude) {
        this.lattitude = lattitude;
    }

    public float getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(float longtitude) {
        this.longtitude = longtitude;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}

