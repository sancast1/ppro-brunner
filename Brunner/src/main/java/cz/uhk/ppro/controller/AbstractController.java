package cz.uhk.ppro.controller;

import java.util.List;
import java.util.Locale;


import cz.uhk.ppro.entity.*;
import cz.uhk.ppro.security.MyUserDetails;
import cz.uhk.ppro.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public abstract class AbstractController {

	@Autowired
	UserService userService;
    @Autowired
    TrainingService trainingService;
    @Autowired
    SegmentService segmentService;
    @Autowired
    PointService pointService;
    @Autowired
    LapService lapService;
    @Autowired
    RecordService recordService;
	
	@Autowired
	MessageSource messageSource;


    User getCurrentUser() {
        MyUserDetails user = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getLogin();
        User result = userService.findUserByLogin(name);
        return result;
    }

	Locale getLocale() {
		return LocaleContextHolder.getLocale();
	}

    void deleteUser(User user) {
        List<Training> trainings = trainingService.findAllTrainingsOfUser(user);

        if (trainings != null){
            for (Training training : trainings) {
                deleteTraining(training);
            }
        }
        userService.deleteUser(user);
    }

    void deleteTraining(Training training){

        List<Record> records = recordService.findAllRecordsOfTraining(training);
        if(records != null){
            recordService.deleteRecords(records);
        }

        List<Lap> laps = lapService.findAllLapsOfTraining(training);
        if( laps != null ){
            lapService.deleteLaps(laps);
        }

        List<Segment> segments = segmentService.getAllSegmentsOfTraining(training);
        if (segments != null){

            for (Segment segment : segments) {

                List<Point> points = pointService.findAllPointsOfSegment(segment);
                if ( points != null ){
                    pointService.deletePoints(points);
                }
            }

            segmentService.deleteSegments(segments);
        }

        trainingService.deleteTraining(training);
    }

}
