package cz.uhk.ppro.controller;


import cz.uhk.ppro.dto.Stats;
import cz.uhk.ppro.dto.dtoRecord;
import cz.uhk.ppro.entity.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/stats")
public class StatsController extends AbstractController {


    @RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
    public String showUserStats(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        Stats stats = trainingService.getUsersStats(getCurrentUser());
        List<Record> records = recordService.findFastestRecordsOfUser(getCurrentUser());
        User user = userService.findUserById(id);

        if ( records == null || stats == null || user == null ) {
            redirectAttributes.addFlashAttribute("error", "No stats were found for you. Add some training first! ;-)");
            return "redirect:/trainings/all";

        } else if( getCurrentUser().getId() != user.getId() && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {

            List<dtoRecord> dtos = new ArrayList<>();
            for (Record record : records) {
                dtos.add(new dtoRecord(record));
            }

            model.addAttribute("records", dtos);
            model.addAttribute("stats", stats);
            return "/stats";
        }


    }









}

