package cz.uhk.ppro.controller;

import cz.uhk.ppro.entity.User;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Locale;

@Controller
@RequestMapping("/profile")
public class ProfileController extends AbstractController {

    private final String BASE_VIEW = "users/";


    @RequestMapping(value = { "detail/{id}" }, method = RequestMethod.GET)
    public String showUserDetail(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        User user = userService.findUserById(id);
        User logged = getCurrentUser();

        if (user == null) {
            redirectAttributes.addFlashAttribute("error", "User was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != id && !logged.getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {
            //model.addAttribute("admin", admin);
            model.addAttribute("user", user);
            return BASE_VIEW + "detail";
        }
    }

    @RequestMapping(value = { "/edit-password/{id}" }, method = RequestMethod.GET)
    public String editUserPassword(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
        User user = userService.findUserById(id);

        if (user == null) {
            redirectAttributes.addFlashAttribute("error", "User was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != id && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {
            model.addAttribute("user", user);
            return BASE_VIEW + "edit-password";
        }
    }

    @RequestMapping(value = { "/edit-password/{id}" }, method = RequestMethod.POST)
    public String updateUserPassword(@PathVariable int id, @Validated(User.ValidationPassword.class) User user, BindingResult result, RedirectAttributes redirectAttributes) {

        Locale locale = LocaleContextHolder.getLocale();

        if (userService.findUserById(id) == null) {
            redirectAttributes.addFlashAttribute("error", "User was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != id && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {

            if(!user.getPassword().equals(user.getConfirmPassword())) {
                result.addError(new FieldError("user", "password", messageSource.getMessage("non.equal.passwords", new String[]{}, locale)));
            }

            if (result.hasErrors()) {
                return BASE_VIEW + "edit-password";
            }

            userService.updateUserPassword(user);

            redirectAttributes.addFlashAttribute("success", "Password of user '" + userService.findUserById(id).getLogin() + "' was successfully changed.");
            return "redirect:/" + "trainings/all";
        }
    }


    @RequestMapping(value = { "edit/{id}" }, method = RequestMethod.GET)
    public String editUser(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
        User user = userService.findUserById(id);

        if (user == null) {
            redirectAttributes.addFlashAttribute("error", "User was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != id && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        }  else {
            boolean self = getCurrentUser().getId() == id;
            model.addAttribute("self", self);
            model.addAttribute("user", user);
            return BASE_VIEW + "edit";
        }
    }

    @RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.POST)
    public String updateUser(@PathVariable int id, @Validated(User.ValidationUpdateUser.class) User user, BindingResult result, RedirectAttributes redirectAttributes) {

        Locale locale = LocaleContextHolder.getLocale();

        if ( userService.findUserById(id) == null ) {
            redirectAttributes.addFlashAttribute("error", "User was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != id && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {
            if ( !userService.isUserLoginUnique(user.getId(), user.getLogin()) ) {
                result.addError(new FieldError("user", "login",
                        messageSource.getMessage("non.unique.login", new String[]{user.getLogin()}, locale))
                );
            }

            if (result.hasErrors()) {
                return BASE_VIEW + "edit";
            }

            userService.updateUser(user);
            user = userService.findUserById(id);

            redirectAttributes.addFlashAttribute("success", "User '" + user.getLogin() + "' was successfully updated.");
            return "redirect:/profile/detail/"+id;
        }
    }





}
