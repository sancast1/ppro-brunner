package cz.uhk.ppro.controller;


import cz.uhk.ppro.dto.*;
import cz.uhk.ppro.entity.*;
import cz.uhk.ppro.validator.GPSTrainingDtoValidator;
import cz.uhk.ppro.validator.ManualTrainingDtoValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

import static cz.uhk.ppro.configuration.Constants.GOOGLE_API_KEY;


@Controller
@RequestMapping("/trainings")
public class TrainingController extends AbstractController {

    private final String BASE_VIEW = "trainings/";

    @Autowired
    ManualTrainingDtoValidator manualTrainingDtoValidator;
    @Autowired
    GPSTrainingDtoValidator gpsTrainingDtoValidator;

    @RequestMapping(value = { "", "/", "/all" }, method = RequestMethod.GET)
    public String listAllTrainingsOfCurrentUser(ModelMap model) {

        List<Training> trainings = trainingService.findAllTrainingsOfUser(getCurrentUser());

        model.addAttribute("self", true);
        model.addAttribute("trainings", trainings);
        return BASE_VIEW + "all";
    }

    @RequestMapping(value = { "/user/{id}" }, method = RequestMethod.GET)
    public String listAllTrainingsOfUserWithId(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
        User user = userService.findUserById(id);

        if (user == null) {
            redirectAttributes.addFlashAttribute("error", "User was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != id && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {
            List<Training> trainings = trainingService.findAllTrainingsOfUser(user);

            boolean self = getCurrentUser().getId() == id;
            model.addAttribute("self", self);
            model.addAttribute("trainings", trainings);
            return BASE_VIEW + "all";
        }
    }


    @RequestMapping(value = { "detail/{id}" }, method=RequestMethod.GET)
    public String showTrainingDetail(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        Training training = trainingService.findTrainingById(id);
        User logged = getCurrentUser();

        if (training == null) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != training.getUser().getId() && !logged.getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {

            boolean self = getCurrentUser().getId() == training.getUser().getId();
            int ownerId = training.getUser().getId();

            if (training.getType() == Training.Type.GPS){

                // get records
                List<Record> records = recordService.findAllRecordsOfTraining(training);
                List<dtoRecord> dtoRecords = new ArrayList<>();
                if ( records != null ){
                    for (Record record : records) {
                        dtoRecords.add(new dtoRecord(record));
                    }
                }

                // get laps
                List<Lap> laps = lapService.findAllLapsOfTrainingOfType(training, Lap.Type.m1000);
                List<dtoLap> dtoLaps = new ArrayList<>();
                for (Lap lap : laps) {
                    dtoLaps.add(new dtoLap(lap));
                }

                // get points
                List<Point> points = pointService.findAllPointsOfTraining(training);
                List<dtoPoint> dtoPoints = new ArrayList<>();
                for (Point point : points) {
                    dtoPoints.add(new dtoPoint(point));
                }


                model.addAttribute("ownerId", ownerId);
                model.addAttribute("self", self);
                model.addAttribute("training", training);
                model.addAttribute("records", dtoRecords);
                model.addAttribute("laps", dtoLaps);
                model.addAttribute("points", dtoPoints);
                model.addAttribute("apiKey", GOOGLE_API_KEY);
                return BASE_VIEW + "detailGPS";

            } else {

                // TODO: dodělat training detail

                model.addAttribute("ownerId", ownerId);
                model.addAttribute("self", self);
                model.addAttribute("training", training);
                return BASE_VIEW + "detail";
            }
        }
    }

    @RequestMapping(value = { "detail/{id}/laps" }, method=RequestMethod.GET)
    public String showTrainingDetailLaps(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        Training training = trainingService.findTrainingById(id);
        User logged = getCurrentUser();

        if (training == null || training.getType() != Training.Type.GPS) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != training.getUser().getId() && !logged.getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {

            List<dtoLapList> allLaps = new ArrayList<>();

            for (Lap.Type type : Lap.Type.values()) {

                List<Lap> laps = lapService.findAllLapsOfTrainingOfType(training, type);
                List<dtoLap> dtoLaps = new ArrayList<>();

                for (Lap lap : laps) {
                    dtoLaps.add(new dtoLap(lap));
                }

                allLaps.add(new dtoLapList(dtoLaps, type));
            }


            boolean self = getCurrentUser().getId() == training.getUser().getId();
            model.addAttribute("self", self);

            int ownerId = training.getUser().getId();
            model.addAttribute("ownerId", ownerId);

            model.addAttribute("training", training);
            model.addAttribute("allLaps", allLaps);
            return BASE_VIEW + "laps";
        }
    }

    @RequestMapping(value = { "map/{id}" }, method=RequestMethod.GET)
    public String showTrainingMap(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        Training training = trainingService.findTrainingById(id);
        User logged = getCurrentUser();

        if (training == null || training.getType() == Training.Type.MANUAL) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if( getCurrentUser().getId() != training.getUser().getId() && !logged.getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {

            List<Point> points = pointService.findAllPointsOfTraining(training);
            List<dtoPoint> dtos = new ArrayList<>();

            for (Point point : points) {
                dtos.add(new dtoPoint(point));
            }

            //String json = new Gson().toJson(dtos);
            //model.addAttribute("json", json);

            model.addAttribute("apiKey", GOOGLE_API_KEY);
            model.addAttribute("points", dtos);
            return BASE_VIEW + "map";
        }
    }


    @RequestMapping(value = { "/new" }, method = RequestMethod.GET)
    public String newManualTraining(ModelMap model) {

        model.addAttribute("training", new dtoManualTraining());

        return BASE_VIEW + "new";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
    public String saveManualTraining(@Validated(ManualTrainingDtoValidator.class) dtoManualTraining training, BindingResult result, RedirectAttributes redirectAttributes, ModelMap model) {

        manualTrainingDtoValidator.validate(training, result);

        if (result.hasErrors()) {
            List<ObjectError> errors =  result.getAllErrors();

            for (ObjectError error : errors) {
                model.addAttribute("error",  error.getCode() );
            }

            model.addAttribute("training", training);
            return BASE_VIEW + "new";
        }

        Training entity = new Training(training);
        entity.setUser(getCurrentUser());
        entity.setType(Training.Type.MANUAL);

        trainingService.saveTraining(entity);

        redirectAttributes.addFlashAttribute("success", "Training '" + entity.getName() + "' was registered successfully.");
       return "redirect:/" + BASE_VIEW;
    }


    @RequestMapping(value = { "edit/{id}" }, method = RequestMethod.GET)
    public String editManualTraining(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        Training training = trainingService.findTrainingById(id);

        if (training == null || training.getType() == Training.Type.GPS) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        }  else if( getCurrentUser().getId() != training.getUser().getId() && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        }else{

            int ownerId = training.getUser().getId();
            model.addAttribute("ownerId", ownerId);

            boolean self = getCurrentUser().getId() == training.getUser().getId();
            model.addAttribute("self", self);

            dtoManualTraining dto = new dtoManualTraining(training);
            model.addAttribute("training", dto);
            return BASE_VIEW + "edit";
        }


    }

    @RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.POST)
    public String updateManualTraining(@PathVariable int id, @Validated(ManualTrainingDtoValidator.class) dtoManualTraining training, BindingResult result, RedirectAttributes redirectAttributes, ModelMap model) {

        manualTrainingDtoValidator.validate(training, result);

        if ( trainingService.findTrainingById(id) == null ) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if (result.hasErrors()) {
            List<ObjectError> errors =  result.getAllErrors();

            for (ObjectError error : errors) {
                model.addAttribute("error",  error.getCode() );
            }

            model.addAttribute("training", training);
            return BASE_VIEW + "edit";
        }

        Training t = trainingService.findTrainingById(id);

        Training entity = new Training(training);
        entity.setUser(t.getUser());

        trainingService.updateTraining(entity);

        redirectAttributes.addFlashAttribute("success", "Training '" + entity.getName() + "' was successfully updated.");
        return "redirect:/" + BASE_VIEW;

    }


    @RequestMapping(value = { "editGPS/{id}" }, method = RequestMethod.GET)
    public String editGPSTraining(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {

        Training training = trainingService.findTrainingById(id);

        if (training == null || training.getType() == Training.Type.MANUAL) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        }  else if( getCurrentUser().getId() != training.getUser().getId() && !getCurrentUser().getRole().name().equals("ROLE_ADMIN") ){
            return "redirect:/403";

        } else {

            int ownerId = training.getUser().getId();
            model.addAttribute("ownerId", ownerId);

            boolean self = getCurrentUser().getId() == training.getUser().getId();
            model.addAttribute("self", self);

            dtoGPXTraining dto = new dtoGPXTraining(training);
            model.addAttribute("training", dto);
            return BASE_VIEW + "editGPS";
        }
    }

    @RequestMapping(value = { "/editGPS/{id}" }, method = RequestMethod.POST)
    public String updateGPSTraining(@PathVariable int id, @Validated(GPSTrainingDtoValidator.class) dtoGPXTraining training, BindingResult result, RedirectAttributes redirectAttributes, ModelMap model) {

        gpsTrainingDtoValidator.validate(training, result);

        if ( trainingService.findTrainingById(id) == null ) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        } else if (result.hasErrors()) {
            List<ObjectError> errors =  result.getAllErrors();

            for (ObjectError error : errors) {
                model.addAttribute("error",  error.getCode() );
            }

            model.addAttribute("training", training);
            return BASE_VIEW + "edit";
        }

        Training t = trainingService.findTrainingById(id);

        Training entity = new Training(training);
        entity.setUser(t.getUser());

        trainingService.updateTraining(entity);

        redirectAttributes.addFlashAttribute("success", "Training '" + entity.getName() + "' was successfully updated.");
        return "redirect:/" + BASE_VIEW;

    }


    @RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
    public String deleteTrainingGET(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
        Training training = trainingService.findTrainingById(id);

        if (training == null) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");
            return "redirect:/" + BASE_VIEW;

        } else {

            int ownerId = training.getUser().getId();
            model.addAttribute("ownerId", ownerId);

            boolean self = getCurrentUser().getId() == training.getUser().getId();
            model.addAttribute("self", self);

            model.addAttribute("training", training);
            return BASE_VIEW + "delete";
        }
    }

    @RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.POST)
    public String deleteTrainingPOST(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
        Training training = trainingService.findTrainingById(id);

        if (training == null) {
            redirectAttributes.addFlashAttribute("error", "Training was not found!");

        } else {
            deleteTraining(training);
            //trainingService.deleteTraining(training);
            redirectAttributes.addFlashAttribute("success", "Training '" + training.getName() + "' was successfully deleted.");
        }

        return "redirect:/" + BASE_VIEW;
    }


}

