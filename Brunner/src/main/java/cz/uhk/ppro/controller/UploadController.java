package cz.uhk.ppro.controller;

import cz.uhk.ppro.dto.dtoGPXTraining;
import cz.uhk.ppro.entity.*;
import cz.uhk.ppro.utils.GPXSaxHandler;
import cz.uhk.ppro.validator.FileValidator;
import cz.uhk.ppro.validator.GPSTrainingDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;

import javax.validation.Valid;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static cz.uhk.ppro.configuration.Constants.UPLOAD_LOCATION;



@Controller
@RequestMapping("/trainings")
public class UploadController extends AbstractController {

    private final String BASE_VIEW = "trainings/";

    @Autowired
    FileValidator fileValidator;
    @Autowired
    GPSTrainingDtoValidator gpsTrainingDtoValidator;

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        binder.addValidators(fileValidator);
    }


    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public String uploadGPXTraining(ModelMap model) {

        dtoGPXTraining training = new dtoGPXTraining();
        model.addAttribute("training", training);

        return BASE_VIEW + "upload";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String saveGPXTraining(@Valid dtoGPXTraining training, BindingResult result, RedirectAttributes redirectAttributes, ModelMap model) throws IOException {

        gpsTrainingDtoValidator.validate(training, result);

        if ( result.hasErrors() ) {

            List<ObjectError> errors =  result.getAllErrors();

            for (ObjectError error : errors) {
                model.addAttribute("error",  error.getCode() );
            }

            model.addAttribute("training", training);
            return BASE_VIEW + "upload";

        } else {

            MultipartFile multipartFile = training.getFile();
            File file = new File( UPLOAD_LOCATION + training.getFile().getOriginalFilename() );
            FileCopyUtils.copy(training.getFile().getBytes(), file);

            boolean success = processGPXFile(training, file);

            if (success) {
                redirectAttributes.addFlashAttribute("success", "Training '" + training.getName() + "' from file '" + multipartFile.getOriginalFilename() + "' was added successfully");
            } else {
                redirectAttributes.addFlashAttribute("error", "An error occurred while processing file with training.");
            }

            return "redirect:/" + BASE_VIEW + "all";
        }
    }


    private boolean processGPXFile(dtoGPXTraining dto, File file){

        try {
            File inputFile = file;
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            GPXSaxHandler handler = new GPXSaxHandler();
            saxParser.parse(inputFile, handler);

            Training training = handler.getTrack();
            training.process();

            createTraining(training, dto);

            createLaps(training);

            createRecords(training);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            return false;
        }

        return true;
    }

    private void createTraining(Training training, dtoGPXTraining dto) {

        training.setUser(getCurrentUser());
        training.setType(Training.Type.GPS);
        training.setDate(dto.getDate());
        training.setDescription(dto.getDescription());
        if ( dto.getName().isEmpty() ){
            // set file name as the name of the training
            if (dto.getFile().getOriginalFilename().length() < 50){
                training.setName(dto.getFile().getOriginalFilename().substring(0,dto.getFile().getOriginalFilename().length()-4));
            } else {
                training.setName(dto.getFile().getOriginalFilename().substring(0,49));
            }

        } else {
            training.setName(dto.getName());
        }

        trainingService.saveTraining(training);

        for (Segment segment : training.getTrack()) {
            segmentService.saveSegment(segment);

            for (Point point : segment.getSegment()){
                pointService.createPoint(point);
            }
        }

    }

    private void createLaps(Training training) {

        //Lap.Type[] types = {Lap.Type.m500, Lap.Type.m1000, Lap.Type.m2000};
        Lap.Type[] types = Lap.Type.values();
        int[] lenghts = {500, 1000, 2000};

        for (int i = 0; i < types.length; i++) {
            Lap.Type type = types[i];
            int lenght = lenghts[i];

            List<Lap> laps = training.getLapsByDistance(lenght);
            for (Lap lap : laps) {
                lap.setType(type);
                lapService.createLap(lap);
            }
        }
    }

    private void createRecords(Training training) {
        //Record.Type[] types = Record.Type.values(); (jsou vynechany rekordy podle casu)
        Record.Type[] types = { Record.Type.m400, Record.Type.m800, Record.Type.m1000, Record.Type.m1500, Record.Type.m3000,
                Record.Type.m5000, Record.Type.m10000, Record.Type.Half_Marathon, Record.Type.Marathon };
        int[] lenghts = {400, 800, 1000, 1500, 3000, 5000, 10000, 21100, 42200};

        for (int i = 0; i < types.length; i++) {
            Record.Type type = types[i];
            int lenght = lenghts[i];

            Record record = training.getFastestDistance(lenght);

            if (record != null){
                record.setType(type);
                record.setUser(getCurrentUser());
                record.setTraining(training);
                recordService.createRecord(record);
            }
        }

    }


}
