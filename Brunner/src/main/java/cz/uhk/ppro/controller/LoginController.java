package cz.uhk.ppro.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cz.uhk.ppro.configuration.Constants;
import cz.uhk.ppro.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class LoginController extends AbstractController {

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, RedirectAttributes redirectAttributes) {

		ModelAndView model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			//System.out.println("Authorized user:  " + auth.getName());
		}

		if (auth != null && (auth.getPrincipal() instanceof User)) {
			String url = Constants.DEFAULT_PAGE;

			String msg = messageSource.getMessage("login.success", new String[]{}, getLocale());
			redirectAttributes.addFlashAttribute("success", msg);
			model.setViewName("redirect:" + url);
			return model;
		}
		if (error != null) {
			String msg = messageSource.getMessage("login.invalid", new String[]{}, getLocale());
			model.addObject("error", msg);
		}

		if (logout != null) {
			String msg = messageSource.getMessage("login.logout", new String[]{}, getLocale());
			model.addObject("msg", msg);
		}

		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}
}
