package cz.uhk.ppro.controller;


import java.util.List;

import cz.uhk.ppro.entity.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
@RequestMapping("/users")
public class UserController extends AbstractController {

	private final String BASE_VIEW = "users/";


	@RequestMapping(value = { "", "/", "/all" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {
		List<User> users = userService.findAllUsers();

		model.addAttribute("users", users);

		return BASE_VIEW + "all";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newUser(ModelMap model) {
		User user = new User();

		model.addAttribute("user", user);
		model.addAttribute("role", User.Role.values());

		return BASE_VIEW + "new";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveUser(@Validated(User.ValidationCreateUser.class) User user, BindingResult result, RedirectAttributes redirectAttributes) {

		if( !user.getPassword().equals(user.getConfirmPassword()) ) {
			result.addError(new FieldError("user", "password",
				messageSource.getMessage("non.equal.passwords", new String[]{}, getLocale()))
			);
		}

		if (!userService.isUserLoginUnique(user.getId(), user.getLogin())) {
			result.addError(new FieldError("user", "login",
				messageSource.getMessage("non.unique.login", new String[]{user.getLogin()}, getLocale()))
			);
		}

		if (result.hasErrors()) {
			return BASE_VIEW + "new";
		}

		userService.saveUser(user);

		redirectAttributes.addFlashAttribute("success", "User '" + user.getLogin() + "' was registered successfully.");
		return "redirect:/" + BASE_VIEW;
	}

	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
	public String deleteUserGET(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id);

		if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");
			return "redirect:/" + BASE_VIEW;

		} else if ( user.getId() == getCurrentUser().getId() ){
            redirectAttributes.addFlashAttribute("error", "You can not delete your own account");
            return "redirect:/" + BASE_VIEW;

        } else {
			model.addAttribute("user", user);
			return BASE_VIEW + "delete";
		}
	}

	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.POST)
	public String deleteUserPOST(@PathVariable int id, ModelMap model, RedirectAttributes redirectAttributes) {
		User user = userService.findUserById(id);

        if (user == null) {
			redirectAttributes.addFlashAttribute("error", "User was not found!");

		} else {
			deleteUser(user);
			redirectAttributes.addFlashAttribute("success", "User '" + user.getLogin() + "' was successfully deleted.");
		}

		return "redirect:/" + BASE_VIEW;
	}


}