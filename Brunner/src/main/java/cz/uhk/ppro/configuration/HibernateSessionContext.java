package cz.uhk.ppro.configuration;

import org.hibernate.Session;

public class HibernateSessionContext {
	
	private static volatile Session SESSION_OVERRIDE = null;
	
	public static Session getSession() {
		if (SESSION_OVERRIDE != null) {
			return SESSION_OVERRIDE;
		} else {
			return null;
		}
	}
	
	public static void overrideSession(Session session) {
		SESSION_OVERRIDE = session;
	}
	
	public static boolean isOverridden() {
		return SESSION_OVERRIDE != null;
	}
	
	

}
