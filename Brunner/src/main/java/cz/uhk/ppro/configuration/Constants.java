package cz.uhk.ppro.configuration;

public abstract class Constants {

	public static final int BCRYPT_STRENGTH = 11;

	public static final String DEFAULT_PAGE = "/trainings";

	public static final String UPLOAD_LOCATION = "D:\\upload/"; // Temporary location where files will be stored

	public static final long MAX_FILE_SIZE = 5242880; // 5MB : Max file size.
	// Beyond that size spring will throw exception.

	public static final long MAX_REQUEST_SIZE = 20971520; // 20MB : Total request size containing Multi part.

	public static final int FILE_SIZE_THRESHOLD = 0; // Size threshold after which files will be written to disk

	public static final String GOOGLE_API_KEY = "AIzaSyAdp8BS2lX-iu-bpuFQMX1mAF1GDstkS4E";
}
