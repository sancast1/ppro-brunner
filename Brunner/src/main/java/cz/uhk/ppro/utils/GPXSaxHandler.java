package cz.uhk.ppro.utils;

import java.time.Instant;

import cz.uhk.ppro.entity.Point;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class GPXSaxHandler extends DefaultHandler {

    private boolean bElevation = false, bTime = false, bHeartRate = false;
    private int iterTP = 1, iterTS = 1;


    private final Training track = new Training();

    private Segment segment = new Segment(0, track);

    private Point point = new Point(0, segment, track);
    
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("trkpt")) {
                 
            String lattitude = attributes.getValue("lat");
            float lat = Float.parseFloat(lattitude);
            point.setLattitude(lat);
            
            String longtitude = attributes.getValue("lon");
            float lon = Float.parseFloat(longtitude);
            point.setLongtitude(lon);
        
        } else if (qName.equalsIgnoreCase("ele")) { 
            bElevation = true;
        } else if (qName.equalsIgnoreCase("time")) { 
            bTime = true;
        } else if (qName.equalsIgnoreCase("gpxtpx:hr")) { 
            bHeartRate = true;
        }
    }

        
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
                     
        // konec trackpointu - uloz trackpoint do tracksegmentu
        if (qName.equalsIgnoreCase("trkpt")) {  
            segment.addTrackPoint(point);
            point = new Point(iterTP++, segment, track);
                                   
         // konec tracksegmentu - uloz do track 
        } else if (qName.equalsIgnoreCase("trkseg")){          
            track.addTrackSegment(segment);
            segment = new Segment(iterTS++, track);
            //iterTP = 0; // order se nenuluje na začátku segmentu
            point = new Point(iterTP++, segment, track);
        } 
    }
      
    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        
        if (bElevation) {   
            String ele = new String(ch, start, length);
            point.setElevation(Float.parseFloat(ele));
            bElevation = false;
 
        } else if (bTime) {
           String time = new String(ch, start, length);         
           point.setTimeStamp(Instant.parse(time));
           bTime = false;
            
        } else if (bHeartRate) {
            String hr = new String(ch, start, length);
            point.setHeartRate(Integer.parseInt(hr));
            bHeartRate = false;            
        }  
    }


    public Training getTrack() {
        return track;
    }


}
