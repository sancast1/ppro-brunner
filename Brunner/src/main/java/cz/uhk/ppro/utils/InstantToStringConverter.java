package cz.uhk.ppro.utils;


import javax.persistence.AttributeConverter;
import java.time.Instant;

public class InstantToStringConverter  implements AttributeConverter<Instant, String> {

    @Override
    public String convertToDatabaseColumn(Instant instant)
    {
        return instant == null ? null : instant.toString();
    }

    @Override
    public Instant convertToEntityAttribute(String dbData)
    {
        return dbData == null ? null : Instant.parse(dbData);
    }

}
