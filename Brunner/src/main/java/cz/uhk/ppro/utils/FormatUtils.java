package cz.uhk.ppro.utils;

import java.time.Duration;


public class FormatUtils {

    public static String convertDurationToStringHHmmss(Duration d) {

        int hh = (int) d.toHours();
        int mm = (int) d.minus(Duration.ofHours(hh)).toMinutes();
        int ss = (int) d.minus(Duration.ofHours(hh)).minus(Duration.ofMinutes(mm)).getSeconds();

        String m = String.format("%02d", mm);
        String s = String.format("%02d", ss);

        return hh + ":" + m + ":" + s;
    }

    public static String convertDurationToStringMMss(Duration d) {

        int mm = (int) d.minus(Duration.ofHours(d.toHours())).toMinutes();
        int ss = (int) d.minus(Duration.ofHours(d.toHours())).minus(Duration.ofMinutes(mm)).getSeconds();

        String m = String.format("%02d", mm);
        String s = String.format("%02d", ss);

        return m + ":" + s;
    }

}
