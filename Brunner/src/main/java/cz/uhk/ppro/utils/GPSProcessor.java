package cz.uhk.ppro.utils;

import cz.uhk.ppro.entity.Point;

import java.time.Duration;

public final class GPSProcessor {

    public static float getDistanceM(Point t1, Point t2) {
        float lat1 = t1.getLattitude();
        float lat2 = t2.getLattitude();
        float lng1 = t1.getLongtitude();
        float lng2 = t2.getLongtitude();

        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                  
        return (float) (earthRadius * c);
    }
    
    
    
    public static Duration getDuration(Point a, Point b) {
        return Duration.between(a.getTimeStamp(), b.getTimeStamp());
    }
    
    
    public static float getElevationDifference(Point t1, Point t2) {
        
        if (t1.getElevation() == 0 || t2.getElevation() == 0 ) {
            return 0;
        } else {

        return (t2.getElevation() - t1.getElevation());
        }      
    }
    
    public static Duration getAveragePace(Duration duration, float distanceMeters){
             
        float m = (float)duration.getSeconds() / 60;
        float km = (float)distanceMeters / 1000;

        if (km == 0){
            return duration.ZERO;
        }

        float minToKm = m/km;
        
        int min = (int) (minToKm % 60);
        float secc = (minToKm % min) * 60;
        int sec = (int) secc;
       
        Duration d = Duration.ofMinutes(min);
        d = d.plusSeconds(sec);
        
        return d;       
    }


    
    
    
    
    
    
    
}
