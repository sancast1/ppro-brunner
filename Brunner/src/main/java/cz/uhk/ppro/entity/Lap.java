package cz.uhk.ppro.entity;

import cz.uhk.ppro.utils.FormatUtils;
import cz.uhk.ppro.utils.GPSProcessor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.format.DateTimeFormatter;


@Entity
@Table( name="lap" )
public class Lap  implements java.io.Serializable {

    public interface ValidationLap { }

    private int id;
    public enum Type {
        m500("500 m"),
        m1000("1000 m"),
        m2000("2000 m");
        private String value;

        Type(String value) { this.value = value; }

        public String getValue() {return value;}

        @Override
        public String toString() {
            return this.getValue();
        }
        public static Type getEnum(String value) {
            for(Type v : values())
                if(v.getValue().equalsIgnoreCase(value)) return v;
            throw new IllegalArgumentException();
        }
    }
    private Type type;
    private int order;
    private boolean complete;
    private Duration duration;
    private float distanceMeters;

    private Duration pace;
    private float elevationGained;
    private int heartRateAvg;

    private Training training;


    @Id
    @Column( name = "id_lap", nullable = false, unique = true )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_training")
    public Training getTraining() { return training; }
    public void setTraining(Training training) { this.training = training; }


    @NotNull( groups = {ValidationLap.class} )
    @Column ( name = "type", nullable = false )
    @Enumerated( EnumType.STRING )
    public Type getType() { return type; }
    public void setType(Type type) { this.type = type; }


    @NotNull( groups = {ValidationLap.class} )
    @Column ( name = "lap_order" )
    public int getOrder() { return order; }
    public void setOrder(int order) { this.order = order; }


    @NotNull( groups = {ValidationLap.class} )
    @Column ( name = "complete" )
    public boolean isComplete() { return complete; }
    public void setComplete(boolean complete) { this.complete = complete; }


    @NotNull( groups = {ValidationLap.class} )
    @Column( name = "duration", nullable = false )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getDuration() { return duration; }
    public void setDuration(Duration duration) { this.duration = duration; }


    @NotNull( groups = {ValidationLap.class} )
    @Column( name = "distance", nullable = false  )
    public float getDistanceMeters() { return distanceMeters; }
    public void setDistanceMeters(float distanceMeters) { this.distanceMeters = distanceMeters; }


    @Column( name = "pace_avg" )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getPace() { return GPSProcessor.getAveragePace(duration, distanceMeters); }
    public void setPace(Duration pace) { this.pace = pace; }


    @Column( name = "elevation_gained" )
    public float getElevationGained() { return elevationGained; }
    public void setElevationGained(float elevationGained) { this.elevationGained = elevationGained; }


    @Column( name = "heart_rate_avg" )
    public int getHeartRateAvg() { return heartRateAvg; }
    public void setHeartRateAvg(int heartRateAvg) { this.heartRateAvg = heartRateAvg; }


    @Override
    public String toString() {
        return "Lap: " + order + " type: " + type.name() + " distance: " + distanceMeters + " time: " + FormatUtils.convertDurationToStringHHmmss(duration) + " complete? " + complete;
    }


    public Lap(){
        id = 0;
        type = Type.m1000;
        order = 0;
        complete = false;
        duration = Duration.ZERO;
        distanceMeters = 0;
        pace = Duration.ZERO;
        elevationGained = 0;
        heartRateAvg = 0;
        training = new Training();

    }

    public Lap(int order, Duration duration, float distanceMeters, float elevationGained, int heartRateAvg, Duration pace, boolean complete, Training training) {
        this();
        this.order = order;
        this.duration = duration;
        this.distanceMeters = distanceMeters;
        this.elevationGained = elevationGained;
        this.heartRateAvg = heartRateAvg;
        this.pace = pace;
        this.complete = complete;
        this.training = training;
    }



}
