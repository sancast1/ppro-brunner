package cz.uhk.ppro.entity;

import cz.uhk.ppro.dto.dtoGPXTraining;
import cz.uhk.ppro.utils.FormatUtils;
import cz.uhk.ppro.dto.dtoManualTraining;
import org.springframework.format.annotation.DateTimeFormat;
import cz.uhk.ppro.utils.*;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table( name="training" )
public class Training implements java.io.Serializable {


    public interface ValidationTraining { }

    private int id;
    public enum Type { MANUAL, GPS }
    private Type type;
    private Date date;
    private String name;
    private String description;
    private Duration duration;
    private Duration pace;
    private Integer distanceMeters;
    private Integer distanceMetersAscent;
    private Integer distanceMetersDescent;
    private Integer elevationGained;
    private Integer elevationAscent;
    private Integer elevationDescent;
    private Integer elevationMin;
    private Integer elevationMax;
    private Integer heartRateAvg;
    private Integer heartRateMax;

    private User user;
    private List<Segment> track;
    private List<Point> points;
    private List<Lap> laps;
    private List<Record> records;

    @Transient
    private int sumHeartRate;



    @Id
    @Column ( name = "id_training", nullable = false, unique = true )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }



    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_user")
    public User getUser() { return user; }
    public void setUser(User user) { this.user = user; }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "training")
    public List<Segment> getTrack() { return track; }
    public void setTrack(List<Segment> track) { this.track = track; }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "training")
    public List<Point> getPoints() { return points; }
    public void setPoints(List<Point> points) { this.points = points; }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "training")
    public List<Lap> getLaps() { return laps; }
    public void setLaps(List<Lap> laps) { this.laps = laps; }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "training")
    public List<Record> getRecords() { return records; }
    public void setRecords(List<Record> records) { this.records = records; }


    @NotNull( groups = {ValidationTraining.class} )
    @DateTimeFormat( pattern="dd.MM.yyyy" )
    @Column( name = "date", nullable = false, columnDefinition="DATETIME" )
    public Date getDate() { return date;  }
    public void setDate(Date date) { this.date = date; }


    @NotNull( groups = {ValidationTraining.class} )
    @Column ( name = "type", nullable = false )
    @Enumerated( EnumType.STRING )
    public Type getType() { return type; }
    public void setType(Type type) { this.type = type; }


    @Size( max = 50, groups = {ValidationTraining.class} )
    @Column( name = "name" )
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }


    @Size( max = 500, groups = {ValidationTraining.class} )
    @Column( name = "description" )
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }


    @NotNull( groups = {ValidationTraining.class} )
    @Column( name = "duration", nullable = false )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getDuration() { return duration; }
    public void setDuration(Duration duration) { this.duration = duration; }


    @NotNull( groups = {ValidationTraining.class} )
    @Min( value = 1 )
    @Column( name = "distance", nullable = false  )
    public Integer getDistanceMeters() { return distanceMeters; }
    public void setDistanceMeters(Integer distanceMeters) { this.distanceMeters = distanceMeters; }


    @NotNull( groups = {ValidationTraining.class} )
    @Column( name = "pace_avg" )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getPace() { return GPSProcessor.getAveragePace(duration, distanceMeters); }
    public void setPace(Duration pace) { this.pace = pace; }


    @Column( name = "distance_ascent" )
    public Integer getDistanceMetersAscent() { return distanceMetersAscent; }
    public void setDistanceMetersAscent(Integer distanceMetersAscent) { this.distanceMetersAscent = distanceMetersAscent; }


    @Column( name = "distance_descent" )
    public Integer getDistanceMetersDescent() { return distanceMetersDescent; }
    public void setDistanceMetersDescent(Integer distanceMetersDescent) { this.distanceMetersDescent = distanceMetersDescent; }


    @Column( name = "elevation_gained" )
    public Integer getElevationGained() { return elevationGained; }
    public void setElevationGained(Integer elevationGained) { this.elevationGained = elevationGained; }


    @Column( name = "elevation_ascent" )
    public Integer getElevationAscent() { return elevationAscent; }
    public void setElevationAscent(Integer elevationAscent) { this.elevationAscent = elevationAscent; }


    @Column( name = "elevation_descent" )
    public Integer getElevationDescent() { return elevationDescent; }
    public void setElevationDescent(Integer elevationDescent) { this.elevationDescent = elevationDescent; }


    @Column( name = "elevation_min" )
    public Integer getElevationMin() { return elevationMin; }
    public void setElevationMin(Integer elevationMin) { this.elevationMin = elevationMin; }


    @Column( name = "elevation_max" )
    public Integer getElevationMax() { return elevationMax; }
    public void setElevationMax(Integer elevationMax) { this.elevationMax = elevationMax; }


    @Column( name = "heart_rate_avg" )
    public Integer getHeartRateAvg() { return heartRateAvg; }
    public void setHeartRateAvg(Integer heartRateAvg) { this.heartRateAvg = heartRateAvg; }


    @Column( name = "heart_rate_max" )
    public Integer getHeartRateMax() { return heartRateMax; }
    public void setHeartRateMax(Integer heartRateMax) { this.heartRateMax = heartRateMax; }



    @Transient
    public String getDurationString() {
        return FormatUtils.convertDurationToStringHHmmss(duration);
    }
    @Transient
    public String getPaceString() {
        return FormatUtils.convertDurationToStringMMss(pace);
    }


    public Training() {
        track = new ArrayList<>();
        elevationGained = 0;
        elevationAscent = 0;
        elevationDescent = 0;
        elevationMax = 0;
        elevationMin = 0;
        distanceMeters = 0;
        distanceMetersAscent = 0;
        distanceMetersDescent = 0;
        heartRateAvg = 0;
        heartRateMax = 0;
        sumHeartRate = 0;
        duration = Duration.ZERO;
        name = "";
        description = "";
        date = new Date();
    }

    public Training(dtoManualTraining dto){
        this();

        this.id = dto.getId();
        this.type = Type.MANUAL;
        this.date = dto.getDate();
        this.duration = dto.getDuration();
        this.distanceMeters = dto.getDistanceMeters();
        this.pace = GPSProcessor.getAveragePace(duration, distanceMeters);
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.distanceMetersAscent = dto.getDistanceMetersAscent();
        this.distanceMetersDescent = dto.getDistanceMetersDescent();
        this.elevationGained = dto.getElevationGained();
        this.elevationAscent = dto.getElevationAscent();
        this.elevationDescent = dto.getElevationDescent();
        this.elevationMin = dto.getElevationMin();
        this.elevationMax = dto.getElevationMax();
        this.heartRateAvg = dto.getHeartRateAvg();
        this.heartRateMax = dto.getHeartRateMax();

    }


    public Training(dtoGPXTraining dto){
        this.id = dto.getId();
        this.type = Type.GPS;
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.date = dto.getDate();
        this.heartRateAvg = dto.getHeartRateAvg();
        this.heartRateMax = dto.getHeartRateMax();
    }


    public void process() {
        int nodes = 0;
        for (Segment t : track) {
            t.process();
            nodes += t.getSegment().size();

            // distance
            distanceMeters += (int) t.getDistanceMeters();
            distanceMetersAscent += (int) t.getDistanceMetersAscent();
            distanceMetersDescent += (int) t.getDistanceMetersDescent();
            // duration
            duration = getDuration().plus(t.getDuration());
            // elevationGained
            elevationGained += (int) t.getElevationGained();
            elevationAscent += (int) t.getElevationAscent();
            elevationDescent -= (int) t.getElevationDescent(); // puvodne zaporne hodnoty - odecitam
            if ( t.getElevationMax() > elevationMax ) elevationMax = (int) t.getElevationMax();
            if ( t.getElevationMin() < elevationMin ) elevationMin = (int) t.getElevationMin();
            // heart rate
            sumHeartRate += t.getHeartRateSum();
            if (heartRateMax < t.getHeartRateMax()) {
                heartRateMax = t.getHeartRateMax();
            }
        }

        // pace
        pace = GPSProcessor.getAveragePace(duration, distanceMeters);

        // average heart rate
        heartRateAvg = sumHeartRate / nodes;
    }

    public Record getFastestDistance(float dist){

        Record best = new Record();

        if (distanceMeters < dist){
            return null;
        }

        Record r = new Record();
        boolean start = true;

        // projdi vsechny body
        for (Segment s : track) {
            for (Point t : s.getSegment()) {

                //najdi pocatecni rekord o dane vzdalenosti
                if ( dist >= r.getDistance() && start) {
                    r.addTrackPoint(t);

                    // ustanoveni prvniho rekordu
                    if (dist < r.getDistance()) {
                        start = false;
                        best = new Record(r.getPoints(),this);
                    }

                    // zkus ho vylepsit
                } else {
                    r.addTrackPoint(t);

                    // pokud nepresahnu meze, nic nedelam
                    if (dist >= r.getDistance()) {

                        if (r.getDuration().getSeconds() < best.getDuration().getSeconds()) {
                            best = new Record(r.getPoints(),this);
                        }

                        // jinak odebiram body od zacatku, dokud nebude vzdalenost v mezich
                    } else {

                        // pokud se odebranim prvniho bodu nedostanu pod zadanou mez, odeberu ho
                        while ( ( (r.getDistance() - r.getPoints().get(0).getdDist() ) >= dist ) ) {
                            r.removeFirstTrackPoint();
                        }

                        if (r.getDuration().getSeconds() < best.getDuration().getSeconds()) {
                            best = new Record(r.getPoints(), this);
                        }

                    }
                }
            }
        }

        return best;
    }

    public List<Lap> getLapsByDuration(Duration dist){
        return getLaps(0, dist);
    }

    public List<Lap> getLapsByDistance(float distance){
        return getLaps(distance, null);
    }

    private List<Lap> getLaps(float d, Duration dur) {
        // rozpoznani typu (vzdalenost X cas)
        boolean bDur = false, bDist=false;
        if (d != 0) { bDist = true; }
        if (dur != null) { bDur = true; }

        List<Lap> laps = new ArrayList<>();
        int lapsCount = 1;
        int nodes = 0;
        float lapDist = 0;
        float lapEle = 0;
        int lapHrSum = 0;
        Duration lapDur = Duration.ZERO;
        Duration lapPace;

        // projdi vsechny body
        for (Segment s : track) {
            for (Point t : s.getSegment()) {
                nodes++;

                lapDist += t.getdDist();
                lapEle += t.getdEle();
                lapHrSum += t.getdHr();
                lapDur = lapDur.plus(t.getdTime());

                // dokoncene kolo (vzdalenost)
                boolean bD = false;
                if (bDist) { bD = lapDist > d; }
                // dokoncene kolo (cas)
                boolean bT = false;
                if (bDur) { bT = dur.minus(lapDur).getSeconds() < 0; }

                if (  bD || bT ) {
                    int hr = lapHrSum / nodes;
                    lapPace = GPSProcessor.getAveragePace(lapDur, lapDist);
                    laps.add(new Lap(lapsCount++, lapDur, lapDist, lapEle, hr, lapPace, true, this));

                    nodes = 0;
                    lapDist = 0;
                    lapEle = 0;
                    lapHrSum = 0;
                    lapDur = Duration.ZERO;
                }
            }
        }

        // posledni nedokoncene kolo
        int hr = lapHrSum / nodes;
        lapPace = GPSProcessor.getAveragePace(lapDur, lapDist);
        laps.add(new Lap(lapsCount++, lapDur, lapDist, lapEle, hr, lapPace, false, this));

        return laps;
    }

    public void addTrackSegment(Segment ts) {

        if (ts.getSegment().size() > 1) {
            track.add(ts);
        }
    }



}

