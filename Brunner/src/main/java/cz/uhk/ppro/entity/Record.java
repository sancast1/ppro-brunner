package cz.uhk.ppro.entity;

import cz.uhk.ppro.utils.GPSProcessor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


@Entity
@Table( name="record" )
public class Record  implements java.io.Serializable, Comparable<Record> {

    public interface ValidationRecord { }

    private int id;
    public enum Type {
        m400("400 m"),
        m800("800 m"),
        m1000("1000 m"),
        m1500("1500 m"),
        m3000("3000 m"),
        m5000("5000 m"),
        m10000("10000 m"),
        Half_Marathon("Half marathon (21000 m)"),
        Marathon("Marathon (42200 m)"),
        One_Hour("One hour"),
        Cooper_Test("Cooper test (12 minutes");
        private String value;

        Type(String value) { this.value = value; }

        public String getValue() {return value;}

        @Override
        public String toString() {
            return this.getValue();
        }
        public static Record.Type getEnum(String value) {
            for(Record.Type v : values())
                if(v.getValue().equalsIgnoreCase(value)) return v;
            throw new IllegalArgumentException();
        }
    }
    private Type type;
    private Duration duration;
    private float distance;
    private Duration pace;

    private Training training;
    private User user;

    // aby bylo možné zobrazit body v mapě, bylo by nutné udržovat many to many vazbu a hlídat pořadí bodu v každém rekordu => zbytečně náročné
    private List<Point> points;


    @Id
    @Column( name = "id_record", nullable = false, unique = true )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_training")
    public Training getTraining() {return training;}
    public void setTraining(Training training) {this.training = training;}


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_user")
    public User getUser() {return user;}
    public void setUser(User user) {this.user = user;}


    @NotNull( groups = {ValidationRecord.class} )
    @Column ( name = "type", nullable = false )
    @Enumerated( EnumType.STRING )
    public Type getType() { return type;}
    public void setType(Type type) {this.type = type;}


    @NotNull( groups = {ValidationRecord.class} )
    @Column( name = "duration", nullable = false )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getDuration() {return duration;}
    public void setDuration(Duration duration) {this.duration = duration;}


    @NotNull( groups = {ValidationRecord.class} )
    @Column( name = "distance", nullable = false  )
    public float getDistance() {return distance;}
    public void setDistance(float distance) {this.distance = distance;}


    @Column( name = "pace_avg" )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getPace() {return GPSProcessor.getAveragePace(duration,distance); }
    public void setPace(Duration pace) {this.pace = pace;}


    @Transient
    public List<Point> getPoints() {return points;}



    public Record() {
        this.points = new ArrayList<>();
        this.distance = 0;
        this.duration = Duration.ZERO;
    }

    public Record(List<Point> points, Training training) {
        this.points = points;
        this.distance = 0;
        this.duration = Duration.ZERO;

        for (Point point : points) {
            distance += point.getdDist();
            duration = duration.plus(point.getdTime());
        }
    }

    protected void addTrackPoint(Point t) {
        points.add(t);
        distance += t.getdDist();
        duration = duration.plus(t.getdTime());
    }

    protected void removeFirstTrackPoint() {
        distance -= points.get(0).getdDist();
        duration = duration.minus(points.get(0).getdTime());
        points.remove(0); 
        
    }

    @Override
    public int compareTo(Record record) {
        // compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than
        // other and 0 if they are supposed to be equal

        // když je THIS menší, vracím -1

        Duration result = duration.minus(record.getDuration());

        if ( result.getSeconds() < 0 ) return -1;
        if ( result.getSeconds() > 0 ) return 1;

        return 0;
    }


    @Override
    public String toString() {
        String sDur = duration.toMinutes() + ":" + ( duration.getSeconds() - (duration.toMinutes() * 60 )  );
        return "Zadana vzdalenost: " + distance + " za " + sDur + " ("+ points.size() +") bodu.";
    }


}
