package cz.uhk.ppro.entity;

import cz.uhk.ppro.utils.GPSProcessor;

import javax.persistence.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table( name="segment" )
public class Segment implements java.io.Serializable {

    private int id;
    private int order;
    private Training training;
    private List<Point> segment;

    private float elevationGained;
    private float elevationAscent;
    private float elevationDescent;
    private float elevationMin;
    private float elevationMax;
    private float distanceMeters;
    private float distanceMetersAscent;
    private float distanceMetersDescent;
    private int heartRateAvg;
    private int heartRateMax;
    private int heartRateSum;
    private Duration duration;


    @Id
    @Column( name = "id_segment", nullable = false, unique = true )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_training")
    public Training getTraining() { return training; }
    public void setTraining(Training training) { this.training = training; }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "segment")
    public List<Point> getSegment() { return segment; }
    public void setSegment(List<Point> segment) { this.segment = segment; }


    @Column ( name = "segment_order", nullable = false )
    public int getOrder() { return order; }
    public void setOrder(int order) { this.order = order; }


    @Transient
    public float getElevationGained() { return elevationGained; }
    public void setElevationGained(float elevationGained) { this.elevationGained = elevationGained; }
    @Transient
    public float getElevationAscent() { return elevationAscent; }
    public void setElevationAscent(float elevationAscent) { this.elevationAscent = elevationAscent; }
    @Transient
    public float getElevationDescent() { return elevationDescent; }
    public void setElevationDescent(float elevationDescent) { this.elevationDescent = elevationDescent; }
    @Transient
    public float getElevationMin() { return elevationMin; }
    public void setElevationMin(float elevationMin) { this.elevationMin = elevationMin; }
    @Transient
    public float getElevationMax() { return elevationMax; }
    public void setElevationMax(float elevationMax) { this.elevationMax = elevationMax; }
    @Transient
    public float getDistanceMeters() { return distanceMeters; }
    public void setDistanceMeters(float distanceMeters) { this.distanceMeters = distanceMeters; }
    @Transient
    public float getDistanceMetersAscent() { return distanceMetersAscent; }
    public void setDistanceMetersAscent(float distanceMetersAscent) { this.distanceMetersAscent = distanceMetersAscent; }
    @Transient
    public float getDistanceMetersDescent() { return distanceMetersDescent; }
    public void setDistanceMetersDescent(float distanceMetersDescent) { this.distanceMetersDescent = distanceMetersDescent; }
    @Transient
    public int getHeartRateAvg() { return heartRateAvg; }
    public void setHeartRateAvg(int heartRateAvg) { this.heartRateAvg = heartRateAvg; }
    @Transient
    public int getHeartRateMax() { return heartRateMax; }
    public void setHeartRateMax(int heartRateMax) { this.heartRateMax = heartRateMax; }
    @Transient
    public int getHeartRateSum() { return heartRateSum; }
    public void setHeartRateSum(int heartRateSum) { this.heartRateSum = heartRateSum; }
    @Transient
    public Duration getDuration() { return duration; }
    public void setDuration(Duration duration) { this.duration = duration; }




    public void process(){
        
        Point a = segment.get(0);
        heartRateSum += a.getHeartRate();
        heartRateMax = a.getHeartRate();
        elevationMax = a.getElevation();
        elevationMin = a.getElevation();

        for (int i = 1; i < segment.size(); i++) {
            Point b = segment.get(i);
                        
            // distance
            float dist = GPSProcessor.getDistanceM(a, b);
            distanceMeters += dist;
            b.setdDist(dist);            
            
            // duration
            Duration dur = GPSProcessor.getDuration(a, b);
            b.setdTime(dur);
            
            // elevation
            float ele = GPSProcessor.getElevationDifference(a, b);
            elevationGained += ele;
            b.setdEle(ele);               
            
            if (ele > 0) { 
                elevationAscent += ele; 
                distanceMetersAscent += dist;
            } else if (ele < 0) { 
                elevationDescent += ele; 
                distanceMetersDescent += dist;
            }

            if ( b.getElevation() > elevationMax ) elevationMax = b.getElevation();
            if ( b.getElevation() < elevationMin ) elevationMin = b.getElevation();
            
            // heart rate
            heartRateSum += b.getHeartRate();
            if ( b.getHeartRate() > heartRateMax ) heartRateMax = b.getHeartRate();
           
            a = b;
        }
        
        // total duration
        Instant first = segment.get(0).getTimeStamp();
        Instant last = segment.get(segment.size() - 1).getTimeStamp();
        duration = Duration.between(first, last);
        
        // average heart rate
        heartRateAvg = heartRateSum / segment.size();

    }

    public Segment(){
        segment = new ArrayList<>();
        elevationGained = 0;
        elevationAscent = 0;
        elevationDescent = 0;
        elevationMax = 0;
        elevationMin = 0;
        distanceMeters = 0;
        distanceMetersAscent = 0;
        distanceMetersDescent = 0;
        heartRateAvg = 0;
        heartRateMax = 0;
        heartRateSum = 0;
    }

    public Segment(int order, Training training) {
        this();

        this.order = order;
        this.training = training;
    }

    public void addTrackPoint( Point t ) { segment.add(t); }




}
