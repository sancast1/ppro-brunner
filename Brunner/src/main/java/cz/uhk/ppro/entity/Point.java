package cz.uhk.ppro.entity;

import cz.uhk.ppro.utils.GPSProcessor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.Instant;

@Entity
@Table(name="point")
public class Point implements java.io.Serializable {

    public interface ValidationPoint { }

    private int id;
    private int order;
    private Float lattitude;
    private Float longtitude;
    private Instant timeStamp;
    private Duration pace;
    private Float elevation;
    private Integer heartRate;
    private Segment segment;
    private Training training;

    private Duration dTime;
    private float dEle;
    private float dDist;
    private int dHr;



    @Id
    @Column( name = "id_point", nullable = false, unique = true )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_segment")
    public Segment getSegment() { return segment; }
    public void setSegment(Segment segment) { this.segment = segment; }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_training")
    public Training getTraining() { return training; }
    public void setTraining(Training training) { this.training = training; }


    @NotNull( groups = {ValidationPoint.class} )
    @Column ( name = "point_order" )
    public int getOrder() { return order; }
    public void setOrder(int order) { this.order = order; }


    @NotNull( groups = {ValidationPoint.class} )
    @Column ( name = "lattitude" )
    public float getLattitude() { return lattitude; }
    public void setLattitude(float lattitude) { this.lattitude = lattitude; }


    @NotNull( groups = {ValidationPoint.class} )
    @Column ( name = "longtitude" )
    public float getLongtitude() { return longtitude; }
    public void setLongtitude(float longtitude) { this.longtitude = longtitude; }


    @NotNull( groups = {ValidationPoint.class} )
    @Column( name = "timestamp" )
    @Convert( converter= cz.uhk.ppro.utils.InstantToStringConverter.class )
    public Instant getTimeStamp() { return timeStamp; }
    public void setTimeStamp(Instant timeStamp) { this.timeStamp = timeStamp; }


    @Column( name = "pace" )
    @Convert( converter= cz.uhk.ppro.utils.DurationToStringConverter.class )
    public Duration getPace() { return GPSProcessor.getAveragePace(dTime, dDist); }
    public void setPace(Duration pace) { this.pace = pace; }


    @Column ( name = "elevation" )
    public Float getElevation() { return elevation; }
    public void setElevation(Float elevation) { this.elevation = elevation; }


    @Column ( name = "heart_rate" )
    public Integer getHeartRate() { return heartRate; }
    public void setHeartRate(Integer heartRate) { this.heartRate = heartRate; }


    @Transient
    public Duration getdTime() { return dTime; }
    public void setdTime(Duration dTime) { this.dTime = dTime; }
    @Transient
    public float getdEle() { return dEle; }
    public void setdEle(float dEle) { this.dEle = dEle; }
    @Transient
    public float getdDist() { return dDist; }
    public void setdDist(float dDist) { this.dDist = dDist; }
    @Transient
    public int getdHr() { return dHr; }
    public void setdHr(int dHr) { this.dHr = dHr; }



    public Point(){
        dDist = 0;
        dEle = 0;
        dHr = 0;
        dTime = Duration.ZERO;

        heartRate = 0;
        order = 0;
        id = 0;
        elevation = Float.valueOf(0);
    }


    public Point(int order, Segment segment, Training training) {
        this();

        this.order = order;
        this.segment = segment;
        this.training = training;
    }

}
