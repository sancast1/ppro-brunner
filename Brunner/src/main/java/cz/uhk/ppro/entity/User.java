package cz.uhk.ppro.entity;


import javax.persistence.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.*;

@Entity
@Table(name="user", uniqueConstraints = @UniqueConstraint(columnNames = "login"))
public class User implements java.io.Serializable {

    public interface ValidationCreateUser { }
    public interface ValidationUpdateUser { }
    public interface ValidationPassword { }

    private int id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    public enum Gender {
        MAN("Man"),
        WOMAN("Woman");
        private String value;

        Gender(String value) { this.value = value; }

        public String getValue() {return value;}

        @Override
        public String toString() {
            return this.getValue();
        }
        public static Gender getEnum(String value) {
            for(Gender v : values())
                if(v.getValue().equalsIgnoreCase(value)) return v;
            throw new IllegalArgumentException();
        }
    }
    private Gender gender;
    private int height;
    private int weight;
    private String login;
    private String password;
    private String confirmPassword;
    public enum Role {
        ROLE_USER("User"),
        ROLE_ADMIN("Admin");
        private String value;

        Role(String value) { this.value = value; }

        public String getValue() {return value;}

        @Override
        public String toString() {
            return this.getValue();
        }
        public static Role getEnum(String value) {
            for(Role v : values())
                if(v.getValue().equalsIgnoreCase(value)) return v;
            throw new IllegalArgumentException();
        }
    }
    private Role role;
    private boolean enabled = true;

    private List<Training> trainings;
    private List<Record> records;


    @Id
    @Column ( name = "id_user", nullable = false, unique = true )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public List<Training> getTrainings() { return trainings; }
    public void setTrainings(List<Training> trainings) { this.trainings = trainings; }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    public List<Record> getRecords() { return records; }
    public void setRecords(List<Record> records) { this.records = records; }


    @Size( min = 2, max = 50, groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @Column( name = "first_name", nullable = false )
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    @Size( min = 2, max = 50, groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @Column( name = "last_name", nullable = false )
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @NotNull( groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @DateTimeFormat( pattern="dd.MM.yyyy" )
    @Column( name = "birth_date", nullable = false, columnDefinition="DATETIME" )
    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


    @Min( value = 0, groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @Column ( name = "height" )
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }


    @Min( value = 0, groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @Column ( name = "weight" )
    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }


    @Enumerated(EnumType.STRING)
    @Column ( name = "gender" )
    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }


    @Size( min = 4, max = 20, groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @Pattern( regexp = "^[\\S]*$", groups = {ValidationUpdateUser.class, ValidationCreateUser.class} )
    @Column ( name = "login", nullable = false, unique= true )
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }


    @NotNull( groups = {ValidationPassword.class} )
    @Size.List ({
            @Size( min = 6, message="The password must be at least {min} characters long.",
                    groups = {ValidationPassword.class, ValidationCreateUser.class}),
            @Size( max = 60, message="The password can not be longer than {max} characters.",
                    groups = {ValidationPassword.class, ValidationCreateUser.class})
    })
    @Column( name = "password", nullable = false )
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }


    @Transient
    public String getConfirmPassword() {return confirmPassword; }
    public void setConfirmPassword(String confirmPassword) { this.confirmPassword = confirmPassword; }


    @Column ( name = "role", nullable = false )
    @Enumerated(EnumType.STRING)
    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }


    @Column ( name = "enabled", nullable = false )
    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Transient
    public int getAge() {
        Years years = Years.yearsBetween(new LocalDate(this.birthDate), new LocalDate());
        return years.getYears();
    }

    @Transient
    public double getBMI() {

        if ( height == 0 ) return 0;

        double w = weight * 1.0;
        double h = height * 0.01;

        return (w / (h * h) );
    }



    @Override
    public String toString() {
        return "USER id: " + id + " login: " + login +" name: " + firstName + " " + lastName;
    }

}
