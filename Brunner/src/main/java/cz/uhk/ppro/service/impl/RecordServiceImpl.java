package cz.uhk.ppro.service.impl;


import cz.uhk.ppro.dao.RecordDao;
import cz.uhk.ppro.entity.Record;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;
import cz.uhk.ppro.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service("recordService")
@Transactional
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordDao dao;


    @Override
    public Record findById(int id) {
        return dao.findById(id);
    }

    @Override
    public void createRecord(Record record) {
        dao.createRecord(record);
    }

    @Override
    public void deleteRecord(Record record) {
        dao.delete(record);
    }

    @Override
    public void deleteRecords(List<Record> records) {
        dao.deleteRecords(records);
    }

    @Override
    public List<Record> findAllRecordsOfTraining(Training training) {
        return dao.findAllRecordsOfTraining(training);
    }


    @Override
    public List<Record> findFastestRecordsOfUser(User user) {

        List<Record> result = new ArrayList<>();

        for (Record.Type type : Record.Type.values()) {

            List<Record> records = dao.findAllRecordsWithTypeOfUser(user, type);

            if ( records != null && ! records.isEmpty() ){
                Collections.sort(records, Record::compareTo);

                result.add(records.get(0));
            }
        }

        return ( ! result.isEmpty() ) ? result : null;

    }
}
