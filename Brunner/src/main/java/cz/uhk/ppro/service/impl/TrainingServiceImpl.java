package cz.uhk.ppro.service.impl;



import cz.uhk.ppro.dao.TrainingDao;
import cz.uhk.ppro.dto.Stats;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;
import cz.uhk.ppro.service.TrainingService;
import cz.uhk.ppro.utils.GPSProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.List;

@Service("trainingService")
@Transactional
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    private TrainingDao dao;


    @Override
    public Training findTrainingById(int id) {
        return dao.findById(id);
    }

    @Override
    public void saveTraining(Training training) {
        dao.createTraining(training);
    }

    @Override
    public void updateTraining(Training training) {
        Training entity = dao.findById(training.getId());

        if (entity != null) {

            //entity.setType(training.getType());
            entity.setDate(training.getDate());
            entity.setName(training.getName());
            entity.setDescription(training.getDescription());

            entity.setHeartRateAvg(training.getHeartRateAvg());
            entity.setHeartRateMax(training.getHeartRateMax());

            //entity.setUser(training.getUser());

            if (training.getType() != Training.Type.GPS){

                entity.setDistanceMeters(training.getDistanceMeters());
                entity.setDuration(training.getDuration());
                entity.setPace(training.getPace());

                entity.setDistanceMetersAscent(training.getDistanceMetersAscent());
                entity.setDistanceMetersDescent(training.getDistanceMetersDescent());

                entity.setElevationGained(training.getElevationGained());
                entity.setElevationAscent(training.getElevationAscent());
                entity.setElevationDescent(training.getElevationDescent());
                entity.setElevationMin(training.getElevationMin());
                entity.setElevationMax(training.getElevationMax());

            }


        }
    }

    @Override
    public void deleteTraining(Training training) {
        dao.delete(training);
    }


    @Override
    public void deleteTrainings(List<Training> trainings) {
        dao.deleteTrainings(trainings);
    }

    @Override
    public List<Training> findAllTrainingsOfUser(User user) {
        return dao.findAllTrainingsOfUser(user);
    }


    // bohužel nejde udržovat v databázi jako automaticky udržovanou tabulku kvůli formátu, ve kterém se ukládá duration
    @Override
    public Stats getUsersStats(User user) {

        List<Training> allTrainings = findAllTrainingsOfUser(user);

        if (allTrainings == null || allTrainings.isEmpty()) {
            return null;
        }

        int distance = 0;
        int distanceGPS = 0;
        int distanceManual = 0;
        Duration duration = Duration.ZERO;
        int distanceMetersAscent = 0;
        int distanceMetersDescent = 0;
        int elevationGained = 0;
        int elevationAscent = 0;
        int elevationDescent = 0;

        int elevationMin = allTrainings.get(0).getElevationMin();
        int elevationMax = allTrainings.get(0).getElevationMax();
        int heartRateMax = allTrainings.get(0).getHeartRateMax();

        Training longestTrainingByDistance = allTrainings.get(0);
        Training longestTrainingByDuration = allTrainings.get(0);
        Training fastestPaceTraining = allTrainings.get(0);

        Duration durationSum = Duration.ZERO;

        for (Training training : allTrainings) {

            distance += training.getDistanceMeters();

            if (training.getType() == Training.Type.GPS){
                distanceGPS += training.getDistanceMeters();
            } else{
                distanceManual +=training.getDistanceMeters();
            }

            duration = duration.plus(training.getDuration());
            elevationGained += training.getElevationGained();
            elevationAscent += training.getElevationAscent();
            elevationDescent += training.getElevationDescent();
            distanceMetersAscent += training.getDistanceMetersAscent();
            distanceMetersDescent += training.getDistanceMetersDescent();

            if (elevationMax < training.getElevationMax()) elevationMax = training.getElevationMax();
            if (elevationMin > training.getElevationMin()) elevationMin = training.getElevationMin();
            if (heartRateMax < training.getHeartRateMax()) heartRateMax = training.getHeartRateMax();

            if (longestTrainingByDistance.getDistanceMeters() < training.getDistanceMeters())
                longestTrainingByDistance = training;
            if (longestTrainingByDuration.getDuration().getSeconds() < training.getDuration().getSeconds())
                longestTrainingByDuration = training;
            if (fastestPaceTraining.getPace().getSeconds() > training.getPace().getSeconds())
                fastestPaceTraining = training;

            durationSum = durationSum.plus(training.getDuration());
        }

        Duration paceAvg = GPSProcessor.getAveragePace(duration, distance);
        int numberOfTrainings = allTrainings.size();
        int distanceAvg = distance / numberOfTrainings;

        long secondsSum = durationSum.getSeconds();
        long secondsOFAvgDuration = secondsSum / numberOfTrainings;
        Duration durationAvg = Duration.ofSeconds(secondsOFAvgDuration);


        return new Stats(distance, duration, paceAvg, distanceMetersAscent, distanceMetersDescent,
                elevationGained, elevationAscent, elevationDescent, elevationMin, elevationMax, heartRateMax,
                numberOfTrainings, distanceAvg, durationAvg, longestTrainingByDistance,
                longestTrainingByDuration, fastestPaceTraining, distanceGPS, distanceManual  );

    }


}