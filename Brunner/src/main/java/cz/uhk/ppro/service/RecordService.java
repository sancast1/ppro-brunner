package cz.uhk.ppro.service;


import cz.uhk.ppro.entity.Record;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;

import java.util.List;
import java.util.Set;

public interface RecordService {

    Record findById(int id);

    void createRecord(Record record);

    void deleteRecord(Record record);

    void deleteRecords(List<Record> records);

    List<Record> findAllRecordsOfTraining(Training training);

    List<Record> findFastestRecordsOfUser(User user);

}
