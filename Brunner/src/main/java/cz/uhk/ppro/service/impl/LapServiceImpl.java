package cz.uhk.ppro.service.impl;

import cz.uhk.ppro.dao.LapDao;
import cz.uhk.ppro.entity.Lap;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.service.LapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("lapService")
@Transactional
public class LapServiceImpl implements LapService {

    @Autowired
    LapDao dao;


    @Override
    public Lap findById(int id) {
        return dao.findById(id);
    }

    @Override
    public void createLap(Lap lap) {
        dao.createLap(lap);
    }

    @Override
    public void deleteLap(Lap lap) {
        dao.delete(lap);
    }

    @Override
    public void deleteLaps(List<Lap> laps) {
        dao.deleteLaps(laps);
    }

    @Override
    public List<Lap> findAllLapsOfTraining(Training training) {
        return dao.findAllLapsOfTraining(training);
    }

    @Override
    public List<Lap> findAllLapsOfTrainingOfType(Training training, Lap.Type type) {
        return dao.findAllLapsOfTrainingOfType(training, type);
    }


}
