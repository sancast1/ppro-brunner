package cz.uhk.ppro.service.impl;

import cz.uhk.ppro.dao.SegmentDao;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.service.SegmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("segmentService")
@Transactional
public class SegmentServiceImpl implements SegmentService {

    @Autowired
    SegmentDao dao;


    @Override
    public Segment findSegmentById(int id) { return dao.findById(id); }

    @Override
    public void saveSegment(Segment segment) { dao.createSegment(segment); }

    @Override
    public void deleteSegment(Segment segment) { dao.delete(segment); }

    @Override
    public void deleteSegments(List<Segment> segments) { dao.deleteSegments(segments); }

    @Override
    public List<Segment> getAllSegmentsOfTraining(Training training) {
        return dao.getAllSegmentsOfTraining(training);
    }

}
