package cz.uhk.ppro.service.impl;

import cz.uhk.ppro.configuration.Constants;
import cz.uhk.ppro.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.ppro.dao.UserDao;
import cz.uhk.ppro.service.UserService;

import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao dao;

	@Override
	public User findUserByLogin(String login) {
		return dao.findUserByLogin(login);
	}

	@Override
	public User findUserById(int id) {
		return dao.findById(id);
	}

	@Override
	public void saveUser(User user) {
		dao.createUser(user);
	}

	@Override
	public void updateUser(User user) {
		User entity = dao.findById(user.getId());

		if (entity != null) {
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			entity.setBirthDate(user.getBirthDate());
			entity.setGender(user.getGender());
			entity.setHeight(user.getHeight());
			entity.setWeight(user.getWeight());

			// occurs when user edits self
			if ( user.getLogin() != null ){
				entity.setLogin(user.getLogin());
			}

			// occurs when admin edits self
			if (user.getRole() != null){
				entity.setRole(user.getRole());
				entity.setEnabled(user.isEnabled());
			}


		}
	}

	@Override
	public void updateUserPassword(User user) {
		User entity = dao.findById(user.getId());

		if (entity != null) {
			String pw_hash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(Constants.BCRYPT_STRENGTH));
			entity.setPassword(pw_hash);
		}
	}

	@Override
	public void deleteUser(User user) {
		dao.deleteUser(user);
	}

	@Override
	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	@Override
	public boolean isUserLoginUnique(Integer id, String login) {
		User user = findUserByLogin(login);
		return ( user == null || ((id != null) && (user.getId() == id)) );
	}


}
