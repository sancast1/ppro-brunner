package cz.uhk.ppro.service.impl;

import cz.uhk.ppro.dao.PointDao;
import cz.uhk.ppro.entity.Point;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("pointService")
@Transactional
public class PointServiceImpl implements PointService {

    @Autowired
    PointDao dao;

    @Override
    public Point findById(int id) { return dao.findById(id); }

    @Override
    public void createPoint(Point point) { dao.createPoint(point); }

    @Override
    public void deletePoint(Point point) { dao.delete(point); }

    @Override
    public void deletePoints(List<Point> points) { dao.deletePoints(points); }

    @Override
    public List<Point> findAllPointsOfSegment(Segment segment) {
        return dao.findAllPointsOfSegment(segment);
    }

    @Override
    public List<Point> findAllPointsOfTraining(Training training) {
        return dao.findAllPointsOfTraining(training);
    }

}
