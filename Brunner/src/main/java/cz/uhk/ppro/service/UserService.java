package cz.uhk.ppro.service;

import cz.uhk.ppro.entity.User;

import java.util.List;


public interface UserService {

	User findUserByLogin(String login);

	User findUserById(int id);

	void saveUser(User user);

	void updateUser(User user);

	void updateUserPassword(User user);

	void deleteUser(User user);

	List<User> findAllUsers();

	boolean isUserLoginUnique(Integer id, String login);


}