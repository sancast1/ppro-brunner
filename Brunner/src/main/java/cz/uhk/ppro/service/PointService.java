package cz.uhk.ppro.service;

import cz.uhk.ppro.entity.Point;
import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;

import java.util.List;


public interface PointService {

    Point findById(int id);

    void createPoint(Point point);

    void deletePoint(Point point);

    void deletePoints(List<Point> points);

    List<Point> findAllPointsOfSegment(Segment segment);

    List<Point> findAllPointsOfTraining(Training training);

}
