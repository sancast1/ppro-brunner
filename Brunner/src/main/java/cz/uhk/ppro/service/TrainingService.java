package cz.uhk.ppro.service;

import cz.uhk.ppro.dto.Stats;
import cz.uhk.ppro.entity.Training;
import cz.uhk.ppro.entity.User;

import java.util.List;


public interface TrainingService {


    Training findTrainingById(int id);

    void saveTraining(Training training);

    void updateTraining(Training training);

    void deleteTraining(Training training);

    void deleteTrainings(List<Training> trainings);

    List<Training> findAllTrainingsOfUser(User user);

    Stats getUsersStats(User user);

}
