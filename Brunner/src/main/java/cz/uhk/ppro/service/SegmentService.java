package cz.uhk.ppro.service;

import cz.uhk.ppro.entity.Segment;
import cz.uhk.ppro.entity.Training;

import java.util.List;


public interface SegmentService {

    Segment findSegmentById(int id);

    void saveSegment(Segment segment);

    void deleteSegment(Segment segment);

    void deleteSegments(List<Segment> segments);

    List<Segment> getAllSegmentsOfTraining(Training training);

}
