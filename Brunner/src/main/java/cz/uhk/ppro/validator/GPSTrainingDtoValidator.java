package cz.uhk.ppro.validator;


import cz.uhk.ppro.dto.*;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class GPSTrainingDtoValidator implements Validator {


    public boolean supports(Class clazz) { return dtoManualTraining.class.equals(clazz); }


    public void validate(Object obj, Errors errors) {

        ValidationUtils.rejectIfEmpty(errors, "date", "Please insert date of your training");

        dtoGPXTraining training = (dtoGPXTraining) obj;

        if (training.getDescription().length() > 500) {
            errors.rejectValue("description", "Description too long (maximum is 500 characters)");

        } else if (training.getName().length() > 50) {
            errors.rejectValue("name", "Name too long (maximum is 50 characters)");

        } else if ( training.getHeartRateMax() < 0 ){
            errors.rejectValue("heartRateMax", "Hearth rate too low for a living person ;-)");

        } else if ( training.getHeartRateAvg() < 0 ){
            errors.rejectValue("heartRateAvg", "Hearth rate too low for a living person ;-)");

        }

    }


}