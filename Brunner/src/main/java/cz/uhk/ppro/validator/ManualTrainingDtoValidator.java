package cz.uhk.ppro.validator;


import cz.uhk.ppro.dto.dtoManualTraining;

import cz.uhk.ppro.entity.Training;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class ManualTrainingDtoValidator implements Validator {


    public boolean supports(Class clazz) { return dtoManualTraining.class.equals(clazz); }


    public void validate(Object obj, Errors e) {

        ValidationUtils.rejectIfEmpty(e, "date", "empty.date");
        ValidationUtils.rejectIfEmpty(e, "durationHours", "empty.durationHours");
        ValidationUtils.rejectIfEmpty(e, "durationMinutes", "empty.durationMinutes");
        ValidationUtils.rejectIfEmpty(e, "durationSeconds", "empty.durationSeconds");
        ValidationUtils.rejectIfEmpty(e, "distanceMeters", "empty.distanceMeters");


        dtoManualTraining training = (dtoManualTraining) obj;

        if ( training.getDurationHours() < 0 ) {
            e.rejectValue("durationHours", "Incorrect time format");

        } else if ( training.getDurationMinutes() < 0 || training.getDurationMinutes() > 60 ){
            e.rejectValue("durationMinutes", "Incorrect time format");

        } else if ( training.getDurationSeconds() < 0  || training.getDurationSeconds() > 60){
            e.rejectValue("durationSeconds", "Incorrect time format");

        } else if ( training.getDistanceMeters() < 1 ){
            e.rejectValue("distanceMeters", "Too short for a propper training ;-)");

        } else if ( training.getDistanceMetersAscent() < 0 ){
            e.rejectValue("distanceMetersAscent", "Too short for a propper training ;-)");

        } else if ( training.getDistanceMetersDescent() < 0 ){
            e.rejectValue("distanceMetersDescent", "Too short for a propper training ;-)");

        } else if ( training.getHeartRateMax() < 0 ){
            e.rejectValue("heartRateMax", "Hearth rate too low for a living person ;-)");

        } else if ( training.getHeartRateAvg() < 0 ){
           e.rejectValue("heartRateAvg", "Hearth rate too low for a living person ;-)");

        } else  if ( training.getType() != Training.Type.MANUAL ){
            e.rejectValue("type", "Wrong training type");

        } else if (training.getDescription().length() > 500) {
            e.rejectValue("description", "Description too long (maximum is 500 characters)");

        } else if (training.getName().length() > 50) {
            e.rejectValue("name", "Name too long (maximum is 50 characters)");

        }

    }


}