package cz.uhk.ppro.validator;

import cz.uhk.ppro.dto.dtoGPXTraining;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class FileValidator implements Validator {



	public boolean supports(Class<?> clazz) {
		return dtoGPXTraining.class.isAssignableFrom(clazz);
	}


	private boolean isFileExtensionCorrect(String fileName){
		/*
		String regex = "([^\\s]+(\\.(?i)(gpx))$)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(file.getName());
		*/

		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extension = fileName.substring(i+1);
		}

        return ( extension.toLowerCase().equals("gpx") ) ?   true :  false;

	}

	public void validate(Object obj, Errors errors) {
		dtoGPXTraining dto = (dtoGPXTraining) obj;

		if( dto.getFile() != null ){

			if (dto.getFile().getSize() == 0) {
				errors.rejectValue("file", "File is missing");

			} else if ( ! isFileExtensionCorrect( dto.getFile().getOriginalFilename() ) ){
				errors.rejectValue("file", "Unrecognized file format. Please upload .gpx file");
			}

		} else {
			errors.rejectValue("file", "missing.file");
		}


	}
}