<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html lang="cs-cz" dir="ltr">
<head>
	<meta charset="UTF-8">
	<link rel='shortcut icon' type='image/x-icon' href='/img/favicon.ico' />
	<tiles:insertAttribute name="header" />
	<title>Brunner | <tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="styles" />
</head>

<body>
<div class="head">
	<div class="container">
		<tiles:insertAttribute name="menu" />
	</div>
</div>
<div class="content">
	<div class="container">
		<tiles:insertAttribute name="messages" />
		<tiles:insertAttribute name="body" />
	</div>
</div>
<footer>
	<div class="container">
		<tiles:insertAttribute name="footer" />
	</div>
</footer>
<tiles:insertAttribute name="scripts" />
<tiles:insertAttribute name="custom-scripts" />
</body>
</html>