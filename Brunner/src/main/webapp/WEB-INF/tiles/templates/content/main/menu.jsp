<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="url" value="${fn:replace(requestScope['javax.servlet.forward.request_uri'],'/Brunner/','')}"/>

<img class="pull-left hidden-xs logo-img" src="<c:url value='/img/logo80.png'/>" >
<img class="pull-left visible-xs logo-img" src="<c:url value='/img/logo40.png'/>" >


<nav class="navigation pull-left">

	<div class="logo">Brunner run tracker</div>

	<ul class="nav navbar-nav hidden-xs">

		<sec:authentication var="principal" property="principal" />

		<c:set var="menu">


			<li <c:if test="${fn:startsWith(url, 'trainings/')}"> active</c:if>">
				<a href="<c:url value='/trainings/' />">
					<spring:message code="menu.trainings" />
				</a>
			</li>

			<li class="dropdown<c:if test="${fn:startsWith(url, 'add/')}"> active</c:if>">
				<a class="dropdown-toggle" data-toggle="dropdown">
					<spring:message code="menu.add" /> <b class="caret"></b>
				</a>

				<ul class="dropdown-menu">
					<li<c:if test="${fn:startsWith(url, 'trainings/new')}"> class="active"</c:if>>
						<a href="<c:url value='/trainings/new' />">
							<spring:message code="menu.add.manual" />
						</a>
					</li>
					<li <c:if test="${fn:startsWith(url, 'trainings/upload/')}"> active</c:if>">
						<a href="<c:url value='/trainings/upload' />">
							<spring:message code="menu.add.file" />
						</a>
					</li>
				</ul>
			</li>

			<li <c:if test="${fn:startsWith(url, 'trainings/stats/')}"> active</c:if>">
				<a href="<c:url value='/stats/${principal.id}' />">
					<spring:message code="menu.stats" />
				</a>
			</li>

			<li class="dropdown<c:if test="${fn:startsWith(url, 'profile/detail')}"> active</c:if>">
				<a class="dropdown-toggle" data-toggle="dropdown">
					<spring:message code="menu.profile" /> <b class="caret"></b>
				</a>

				<ul class="dropdown-menu">
					<li<c:if test="${fn:startsWith(url, 'users/edit')}"> class="active"</c:if>>
						<a href="<c:url value='/profile/detail/${principal.id}' />">
							<spring:message code="menu.profile.detail" />
						</a>
					</li>
					<li<c:if test="${fn:startsWith(url, 'users/edit')}"> class="active"</c:if>>
						<a href="<c:url value='/profile/edit/${principal.id}' />">
							<spring:message code="menu.edit" />
						</a>
					</li>
					<li<c:if test="${fn:startsWith(url, 'profile/edit-password')}"> class="active"</c:if>>
						<a href="<c:url value='/profile/edit-password/${principal.id}' />">
							<spring:message code="menu.passwd" />
						</a>
					</li>
				</ul>
			</li>

			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<li class="dropdown<c:if test="${fn:startsWith(url, 'users/all')}"> active</c:if>">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<spring:message code="menu.admin.users" /> <b class="caret"></b>
					</a>

					<ul class="dropdown-menu">
						<li<c:if test="${fn:startsWith(url, 'users/new')}"> class="active"</c:if>>
							<a href="<c:url value='/users/all' />">
								<spring:message code="menu.admin.users.all" />
							</a>
						</li>
						<li<c:if test="${fn:startsWith(url, 'users/new')}"> class="active"</c:if>>
							<a href="<c:url value='/users/new' />">
								<spring:message code="menu.users.new" />
							</a>
						</li>
					</ul>
				</li>
			</sec:authorize>


		</c:set>
		<c:out value="${menu}" escapeXml="false" />
	</ul>

	<div class="clearfix"></div>

</nav>

<button type="button" class="navbar-toggle" data-toggle="collapse"
		data-target="#menu-navbar-collapse">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>



<sec:authorize access="isAuthenticated()">
	<div class="menu-bar">

		<div class="button user">
			<span class="glyphicon glyphicon-user"></span>
			<div class="user-name">
				<c:set var="username" value="${fn:split(pageContext.request.userPrincipal.name, ' ')}" />
				<a href="<c:url value='/profile/detail/${principal.id}' />">
					<div>${username[0]}</div>
					<div>${username[1]}</div>
				</a>
			</div>
		</div>

		<a href="<c:url value="/logout" />">
			<div class="button log-out">
				<span class="glyphicon glyphicon-off"></span>
			</div>
		</a>

	</div>

</sec:authorize>

<div class="clearfix visible-xs"></div>
<nav class="visible-xs">
	<ul class="collapse nav navbar-nav navbar-collapse" id="menu-navbar-collapse">
		<c:out value="${menu}" escapeXml="false" />
	</ul>
</nav>

