<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url var="jquery" value='/js/jquery-1.12.0.min.js' />
<script type="text/javascript" src="${jquery}"></script>

<c:url var="bootstrap" value='/js/bootstrap.min.js' />
<script type="text/javascript" src="${bootstrap}"></script>

<c:url var="datepicker" value='/js/datepicker.min.js' />
<script type="text/javascript" src="${datepicker}"></script>

<c:url var="datepicker" value='/js/locales/bootstrap-datepicker.cs.min.js' />
<script type="text/javascript" src="${datepicker}"></script>

<c:url var="datepicker" value='/js/locales/bootstrap-datepicker.en-GB.min.js' />
<script type="text/javascript" src="${datepicker}"></script>

<c:url var="table" value='/js/bootstrap-table.min.js' />
<script type="text/javascript" src="${table}"></script>

<c:url var="table" value='/js/sweetalert.min.js' />
<script type="text/javascript" src="${swal}"></script>

<c:url var="table" value='/js/float-label-form.js' />
<script type="text/javascript" src="${flab}"></script>




<script>
$(document).ready(function initDatepicker() {
	var language = (navigator.language || navigator.browserLanguage).split('-')[0];
	var format;
	switch (language) {
		case "cs":
			format = "dd.mm.yyyy";
			break;
		case "en":
		default:
			format = "dd.mm.yyyy";
	}
	$('input.datepicker').datepicker({
	    format: format,
	    weekStart: 1,
	    startDate: "1.1.1900",
	    endDate: "31.12.2099",
	    clearBtn: true,
	    language: language,
	    todayHighlight: true
	});
});



$(document).ready(function initMenu() {
	var menuTimerId;
	$("li.dropdown").mouseover(function() {
		if ($(window).width() > 767) {
			var self = $(this);
			clearTimeout(menuTimerId);
			menuTimerId = setTimeout(function() {
				self.addClass("open");
			}, 50);
		}
	});
	$("li.dropdown").mouseout(function() {
		if ($(window).width() > 767) {
			var self = $(this);
			clearTimeout(menuTimerId);
			menuTimerId = setTimeout(function() {
				self.removeClass("open");	
			}, 250);
		}
	});

});
</script>
<%--
http://eternicode.github.io/bootstrap-datepicker/?markup=input&format=dd.mm.yyyy&weekStart=1&startDate=1.1.2000&endDate=1.1.2100&startView=0&minViewMode=0&maxViewMode=2&todayBtn=false&clearBtn=true&language=cs&orientation=auto&multidate=&multidateSeparator=&todayHighlight=on&keyboardNavigation=on&forceParse=on#sandbox
--%>