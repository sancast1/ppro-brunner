<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html lang="cs-cz" dir="ltr">
<head>
	<meta charset="UTF-8">
	<link rel='shortcut icon' type='image/x-icon' href='/img/favicon.ico' />
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="styles" />
</head>

<body class="login-page">
<div class="content">
	<div class="container">
		<tiles:insertAttribute name="body" />
	</div>
</div>
<footer>
	<div class="container">
		<tiles:insertAttribute name="footer" />
	</div>
</footer>
</body>
</html>