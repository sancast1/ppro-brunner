<%@ tag body-content="tagdependent" isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="name" required="true" rtexprvalue="true" description="Name of corresponding property in bean object"%>
<%@ attribute name="label" required="true" rtexprvalue="true" description="Label"%>
<%@ attribute name="type" required="true" rtexprvalue="true" description="Input type (text/password)"%>
<%@ attribute name="select_null" required="false" rtexprvalue="true" description="If add null option for select, default false"%>
<%@ attribute name="data" required="false" rtexprvalue="true" type="java.util.HashMap" description="Data for select"%>
<%@ attribute name="min" required="false" rtexprvalue="true" description="Mix value"%>
<%@ attribute name="max" required="false" rtexprvalue="true" description="Max value"%>
<%@ attribute name="value" required="false" rtexprvalue="true" description="Default value"%>
<%@ attribute name="disabled" required="false" rtexprvalue="true" description="Is the field disabled?"%>
<%@ attribute name="erros" required="false" rtexprvalue="true" description="Show errors?"%>

<spring:bind path="${name}">
	<div class="row form-group ${status.error ? 'has-error' : ''}">
		<div class="col-md-7 col-sm-8 col-xs-12">
			<div class="form-group float-label-control">

				<label for="${name}">${label}</label>

				<c:choose>

					<%-- Text input --%>
					<c:when test="${type == 'text'}">
						<form:input path="${name}" id="${name}" class="form-control" placeholder="${label}" disabled="${disabled}"/>
					</c:when>

					<%-- Select input --%>
					<c:when test="${type == 'select'}">
						<c:choose>
							<c:when test="${empty data}">
								<form:select path="${name}" id="${name}" class="form-control" placeholder="${label}" disabled="${disabled}">
									<form:options items="${enumValues}" />
								</form:select> 
							</c:when>
							<c:otherwise>
								<form:select path="${name}" id="${name}" class="form-control" placeholder="${label}" disabled="${disabled}">
								<c:if test="${not empty select_null}">
									<form:option value="0" label="-" />
								</c:if>
									<form:options items="${data}" />
								</form:select>
							</c:otherwise>
						</c:choose>
					</c:when>

					<%-- Textarea --%>
					<c:when test="${type == 'textarea'}">
						<form:textarea path="${name}" id="${name}" class="form-control"
									   placeholder="${label}" rows="5" disabled="${disabled}" />
					</c:when>

					<%-- Password input --%>
					<c:when test="${type == 'password'}">
						<form:password path="${name}" id="${name}" class="form-control"
									   placeholder="${label}" disabled="${disabled}"/>
					</c:when>

					<%-- Date input --%>
					<c:when test="${type == 'date'}">
					<form:input path="${name}" id="${name}"  class="datepicker form-control"
								placeholder="${label}" disabled="${disabled}" />
					</c:when>

					<%-- Number input --%>
					<c:when test="${type == 'number'}">
					<form:input path="${name}" id="${name}" class="form-control" type="number"
								value="${value}" min="${min}" max="${max}" placeholder="${label}"
								onkeypress='return event.charCode >= 48 && event.charCode <= 57'
								disabled="${disabled}" />
					</c:when>

				</c:choose>
			</div>
		</div>


		<div class="col-md-5 col-sm-4 col-xs-12">
			<form:errors path="${name}" cssClass="help-block"/>
		</div>



	</div>
</spring:bind>