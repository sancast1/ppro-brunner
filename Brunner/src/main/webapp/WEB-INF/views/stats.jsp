<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="mainTemplate">
    <tiles:putAttribute name="title" value="Statistics" />


    <tiles:putAttribute name="body">

        <h1><spring:message code="stats" /></h1>


        <div class="row">
            <h2><spring:message code="stats.overal" /></h2>

            <div class="col-sm-8 col-xs-12">

                <table class="table table-responsive">
                    <tr>
                        <td><spring:message code="stats.trainings" />:</td>
                        <td><b>${stats.numberOfTrainings}</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.distance" />:</td>
                        <td><b>${stats.distance} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.duration" />:</td>
                        <td><b>${stats.duration}</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.pace" />:</td>
                        <td><b>${stats.pace} min/km</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.distance.avg" />:</td>
                        <td><b>${stats.distanceAvg} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.duration.avg" />:</td>
                        <td><b>${stats.durationAvg}</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.distance.asc" />:</td>
                        <td><b>${stats.distanceMetersAscent} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.distance.desc" />:</td>
                        <td><b>${stats.distanceMetersDescent} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.elevation.gain" />:</td>
                        <td><b>${stats.elevationGained} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.elevation.asc" />:</td>
                        <td><b>${stats.elevationAscent} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.elevation.desc" />:</td>
                        <td><b>${stats.elevationDescent} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.elevation.min" />:</td>
                        <td><b>${stats.elevationMin} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.elevation.max" />:</td>
                        <td><b>${stats.elevationMax} m</b></td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.heart.max" />:</td>
                        <td><b>${stats.heartRateMax} bpm</b></td>
                    </tr>


                </table>


            </div>

            <div id="piechart" class="col-sm-4 col-xs-12" style="height: 220px; margin: auto;" >
                a
            </div>

        </div>


        <div class="row">
            <h2><spring:message code="stats.best" /></h2>


                <table class="table table-responsive">
                    <tr>
                        <td><spring:message code="stats.distance.longest" />:</td>
                        <td><b>${stats.longestTrainingByDistance.distanceMeters} m</b></td>
                        <td>
                            <a data-placement="top" data-toggle="tooltip" title="Detail"
                               class="btn btn-primary btn-xs"
                               href="<c:url value='/trainings/detail/${stats.longestTrainingByDistance.id}' />">
                                <span class="fa fa-line-chart"></span>
                                <span><spring:message code="trainings.detail" /></span>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.duration.longest" />:</td>
                        <td><b>${stats.longestTrainingByDuration.durationString}</b></td>
                        <td>
                            <a data-placement="top" data-toggle="tooltip" title="Detail"
                               class="btn btn-primary btn-xs"
                               href="<c:url value='/trainings/detail/${stats.longestTrainingByDuration.id}' />">
                                <span class="fa fa-line-chart"></span>
                                <span><spring:message code="trainings.detail" /></span>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td><spring:message code="stats.pace.fastest" />:</td>
                        <td><b>${stats.fastestPaceTraining.paceString} min/km</b></td>
                        <td>
                            <a data-placement="top" data-toggle="tooltip" title="Detail"
                               class="btn btn-primary btn-xs"
                               href="<c:url value='/trainings/detail/${stats.fastestPaceTraining.id}' />">
                                <span class="fa fa-line-chart"></span>
                                <span><spring:message code="trainings.detail" /></span>
                            </a>
                        </td>
                    </tr>
                </table>

        </div>



        <div class="row">
            <h2><spring:message code="stats.fastest" /></h2>

            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th data-field="distance" data-sortable="false"><spring:message code="trainings.distance" /></th>

                        <th data-field="duration" data-sortable="false"><spring:message code="trainings.pace" /></th>

                        <th data-field="pace" data-sortable="false"><spring:message code="trainings.duration" /></th>

                        <th data-field="date" data-sortable="false"><spring:message code="trainings.date" /></th>
                    </tr>
                </thead>

                <c:forEach items="${records}" var="rec" >
                    <tr>
                        <td><b>${rec.type.value}</b></td>

                        <td>${rec.pace} min/km</td>

                        <td>${rec.duration}</td>

                        <td><a href="/trainings/detail/${rec.training.id}">
                            <fmt:formatDate value="${rec.training.date}" pattern="dd.MM.yyyy"/>
                        </a></td>
                    </tr>

                </c:forEach>
            </table>

        </div>

    </tiles:putAttribute>

    <tiles:putAttribute name="custom-scripts">

        <script type="text/javascript" src="/js/charts-loader.js"></script>

        <script type="text/javascript">

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {

                var data = google.visualization.arrayToDataTable([
                    ['Origin', 'Distance'],
                    ['GPS-based', ${stats.distanceGPS}],
                    ['Added manually', ${stats.distanceManual}]
                ]);

                var options = {
                    title: 'Total distance according to the training origin'
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, options);
            }

        </script>


    </tiles:putAttribute>

</tiles:insertDefinition>