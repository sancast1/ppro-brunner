<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="mainTemplate">

    <tiles:putAttribute name="header">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    </tiles:putAttribute>

    <tiles:putAttribute name="title" value="Training: ${training.name}" />

    <tiles:putAttribute name="custom-scripts">

        <!-- draw Map -->
        <script>
            function initMap() {

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,

                    center: {lat: <c:out value="${points.get(0).lattitude}" />,  lng: <c:out value="${points.get(0).longtitude}" />},

                    mapTypeId: google.maps.MapTypeId.TERRAIN

                });

                var flightPlanCoordinates = [

                    <c:forEach items="${points}" var="p" >

                    {lat: ${p.lattitude}, lng: ${p.longtitude}},

                    </c:forEach>
                ];

                var flightPath = new google.maps.Polyline({
                    path: flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });

                function zoomToObject(obj){
                    var bounds = new google.maps.LatLngBounds();
                    var points = obj.getPath().getArray();
                    for (var n = 0; n < points.length ; n++){
                        bounds.extend(points[n]);
                    }
                    map.fitBounds(bounds);
                }

                flightPath.setMap(map);
                zoomToObject(flightPath);
            }
        </script>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key= <c:out value="${apiKey}"/>&callback=initMap">
        </script>

    </tiles:putAttribute>

    <tiles:putAttribute name="body">

        <h1>${training.name} (<fmt:formatDate value="${training.date}" pattern="dd.MM.yyyy"/>)</h1>

        <div class="row">

            <div id="general-info" class="col-md-6 col-xs-12">

                <h2> <h2><spring:message code="trainings.general" /></h2></h2>

                <c:if test="${training.description != ''}">
                    <span><b><spring:message code="trainings.desc" />:</b></span>
                    <p>${training.description}</p>
                    <br>
                </c:if>

                <div class="floating-box">
                    <span class="fa fa-road"></span>&nbsp;
                    <spring:message code="trainings.distance" />
                    </br>
                    <b>${training.distanceMeters} m</b>
                </div>

                <div class="floating-box">
                    <span class="fa fa-clock-o"></span>
                    <spring:message code="trainings.duration" />
                    </br>
                    <b>${training.durationString}</b>
                </div>

                <div class="floating-box">
                    <span class="fa fa-line-chart"></span>
                    <spring:message code="trainings.pace" />
                    </br>
                    <b>${training.paceString} min/km</b>
                </div>

                <div class="floating-box">
                    <span class="fa fa-arrow-up"></span>
                    <spring:message code="trainings.distance.asc" />
                    </br>
                    <b>${training.distanceMetersAscent} m</b>
                </div>

                <div class="floating-box">
                    <span class="fa fa-arrow-down"></span>
                    <spring:message code="trainings.distance.desc" />
                    </br>
                    <b>${training.distanceMetersDescent} m</b>
                </div>

                <div class="floating-box">
                    <spring:message code="trainings.elevation.gain" />
                    </br>
                    <b>${training.elevationGained} m</b>
                </div>

                <div class="floating-box">
                    <spring:message code="trainings.elevation.asc" />
                    </br>
                    <b>${training.elevationAscent} m</b>
                </div>

                <div class="floating-box">
                    <spring:message code="trainings.elevation.desc" />
                    </br>
                    <b>${training.elevationDescent} m</b>
                </div>

                <div class="floating-box">
                    <spring:message code="trainings.elevation.max" />
                    </br>
                    <b>${training.elevationMax} m</b>
                </div>

                <div class="floating-box">
                    <spring:message code="trainings.elevation.min" />
                    </br>
                    <b>${training.elevationMin} m</b>
                </div>

                <div class="floating-box">
                    <span class="fa fa-heartbeat"></span>
                    <spring:message code="trainings.heart.avg" />
                    </br>
                    <b>${training.heartRateAvg} bpm</b>
                </div>

                <div class="floating-box">
                    <span class="fa fa-heart"></span>
                    <spring:message code="trainings.heart.max" />
                    </br>
                    <b>${training.heartRateMax} bpm</b>
                </div>


            </div>

            <div id="map" class="col-md-6 col-xs-12">
                map
            </div>

        </div>
        <div class="clear-both"></div>
        <br>


        <div class="row">

            <div id="laps1000" class="col-md-6 col-xs-12">

                <h2><spring:message code="trainings.oneKm" /></h2>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>

                            <th data-field="distanceMeters" data-sortable="false"><spring:message code="trainings.actDist" /></th>

                            <th data-field="durationString" data-sortable="false"><spring:message code="trainings.time" /></th>

                            <th data-field="paceString" data-sortable="false"><spring:message code="trainings.pace" /></th>
                        </tr>
                    </thead>

                    <c:forEach items="${laps}" var="lap">
                        <tr>
                            <td> ${lap.order}.</td>

                            <td> ${lap.distanceMeters}</td>

                            <td> ${lap.durationString}</td>

                            <td> ${lap.paceString}</td>
                        </tr>
                    </c:forEach>
                </table>

                <a href="/trainings/detail/${training.id}/laps"><spring:message code="trainings.allLaps" /></a>

            </div>

            <div id="records" class="col-md-6 col-xs-12">

                <h2><spring:message code="trainings.records" />:</h2>

                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th data-field="distance" data-sortable="false"><spring:message code="trainings.distance" /></th>

                            <th data-field="pace" data-sortable="false"><spring:message code="trainings.pace" /></th>

                            <th data-field="duration" data-sortable="false"><spring:message code="trainings.duration" /></th>
                        </tr>
                    </thead>

                    <c:forEach items="${records}" var="rec" >
                        <tr>
                            <td><b>${rec.type.value}</b></td>

                            <td>${rec.pace} min/km</td>

                            <td>${rec.duration}</td>
                        </tr>

                    </c:forEach>
                </table>

            </div>

        </div>

        <div class="clear-both"></div>
        <br>

        <div class="row">

            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.delete" />"
               class="btn btn-danger btn-lg"
               href="<c:url value='/trainings/delete/${training.id}' />">
                <span class="fa fa-trash"></span>
                <span><spring:message code="trainings.delete.button" /></span> </a>

            &nbsp;

            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.delete" />"
               class="btn btn-warning btn-lg"
               href="<c:url value='/trainings/editGPS/${training.id}' />">
                <span class="fa fa-pencil"></span>
                <span><spring:message code="trainings.edit" /></span> </a>

            &nbsp;

            <c:choose>
                <c:when test="${self}">
                    <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                       class="btn btn-primary btn-lg"
                       href="<c:url value='/trainings/' />">
                        <span class="fa fa-heartbeat"></span>
                        <span><spring:message code="trainings.back" /></span> </a>
                </c:when>

                <c:otherwise>
                    <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                       class="btn btn-primary btn-lg"
                       href="<c:url value='/trainings/user/${ownerId}' />">
                        <span class="fa fa-heartbeat"></span>
                        <span><spring:message code="trainings.back" /></span> </a>
                </c:otherwise>

            </c:choose>

        </div>

    </tiles:putAttribute>

</tiles:insertDefinition>