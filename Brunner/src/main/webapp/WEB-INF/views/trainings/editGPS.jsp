<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


    <tiles:insertDefinition name="mainTemplate">
    <tiles:putAttribute name="title" value="Edit training: ${training.name}" />
    <tiles:putAttribute name="body">

        <h1><spring:message code="trainings.edit.h2" /></h1>

        <form:form method="POST" modelAttribute="training">

            <div class="row" style="margin: 15px;">

                <div class="col-md-7 col-xs-12">

                    <spring:message code="trainings.name" var="name" />
                    <tag:inputField name="name" label="${name}" type="text"/>

                    <spring:message code="trainings.date" var="date" />
                    <tag:inputField name="date" label="${date}" type="date"/>

                    <spring:message code="trainings.desc" var="desc" />
                    <tag:inputField name="description" label="${desc}" type="textarea"/>

                    <spring:message code="trainings.heart.avg" var="heartAvg" />
                    <tag:inputField name="heartRateAvg" label="${heartAvg} (bpm)" type="number" min="0"/>

                    <spring:message code="trainings.heart.max" var="heartMax" />
                    <tag:inputField name="heartRateMax" label="${heartMax} (bpm)" type="number" min="0"/>

                    <br>

                    <button type="submit" class="btn btn-lg btn-success"><spring:message code="trainings.update" /></button>

                    &nbsp;

                    <c:choose>
                        <c:when test="${self}">
                            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                               class="btn btn-primary btn-lg"
                               href="<c:url value='/trainings/' />">
                                <span class="fa fa-heartbeat"></span>
                                <span><spring:message code="trainings.back" /></span> </a>
                        </c:when>

                        <c:otherwise>
                            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                               class="btn btn-primary btn-lg"
                               href="<c:url value='/trainings/user/${ownerId}' />">
                                <span class="fa fa-heartbeat"></span>
                                <span><spring:message code="trainings.back" /></span> </a>
                        </c:otherwise>
                    </c:choose>

                </div>
            </div>
        </form:form>

    </tiles:putAttribute>


    <tiles:putAttribute name="custom-scripts">
        <tag:tooltipInit/>
    </tiles:putAttribute>

</tiles:insertDefinition>