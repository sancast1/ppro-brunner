<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<tiles:insertDefinition name="mainTemplate">
    <tiles:putAttribute name="title" value="New training" />
    <tiles:putAttribute name="body">

         <h1><spring:message code="trainings.new" /></h1>

         <form:form method="POST" modelAttribute="training" enctype="multipart/form-data" class="form-horizontal">

             <div class="row" style="margin: 15px;">
                 <div class="col-md-7 col-xs-12">

                     <spring:message code="trainings.file" var="file" />
                     <label for="file">${file}</label>
                     <div class="input-group">
                         <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Browse&hellip;
                                    <form:input type="file" path="file" id="file" class="form-control" cssStyle="display: none;" placeholder="file" />
                                </span>
                         </label>
                         <input type="text" class="form-control" readonly>
                     </div>
                     <span class="help-block">Please select .gpx file containing your training info.</span>

                     <br>

                    <spring:message code="trainings.name" var="name" />
                    <tag:inputField name="name" label="${name}" type="text"/>

                    <spring:message code="trainings.date" var="date" />
                    <tag:inputField name="date" label="${date}" type="date"/>

                    <spring:message code="trainings.desc" var="desc" />
                    <tag:inputField name="description" label="${desc}" type="textarea"/>

                    <button type="submit" class="btn btn-lg btn-success"><spring:message code="trainings.save" /></button>

                     &nbsp;

                     <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                        class="btn btn-primary btn-lg"
                        href="<c:url value='/trainings/' />">
                         <span class="fa fa-heartbeat"></span>
                         <span><spring:message code="trainings.back" /></span> </a>

                </div>
            </div>
         </form:form>

        <br/>

    </tiles:putAttribute>


    <tiles:putAttribute name="custom-scripts">

        <tag:tooltipInit/>

        <script>
            $(function() {

                // We can attach the `fileselect` event to all file inputs on the page
                $(document).on('change', ':file', function() {
                    var input = $(this),
                            numFiles = input.get(0).files ? input.get(0).files.length : 1,
                            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });

                // We can watch for our custom `fileselect` event like this
                $(document).ready( function() {
                    $(':file').on('fileselect', function(event, numFiles, label) {

                        var input = $(this).parents('.input-group').find(':text'),
                                log = numFiles > 1 ? numFiles + ' files selected' : label;

                        if( input.length ) {
                            input.val(log);
                        } else {
                            if( log ) alert(log);
                        }

                    });
                });

            });

        </script>

    </tiles:putAttribute>

</tiles:insertDefinition>