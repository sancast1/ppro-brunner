<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>

<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Brunner | Fullscreen map</title>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            height: 100%;
        }
    </style>
</head>

<body>

    <div id="map"></div>

    <script>

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,

                center: {lat: <c:out value="${points.get(0).lattitude}" />,  lng: <c:out value="${points.get(0).longtitude}" />},

                mapTypeId: google.maps.MapTypeId.TERRAIN
            });

            var flightPlanCoordinates = [

                <c:forEach items="${points}" var="p" >
                    {lat: ${p.lattitude}, lng: ${p.longtitude}},
                </c:forEach>

            ];

            var flightPath = new google.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            function zoomToObject(obj){
                var bounds = new google.maps.LatLngBounds();
                var points = obj.getPath().getArray();
                for (var n = 0; n < points.length ; n++){
                    bounds.extend(points[n]);
                }
                map.fitBounds(bounds);
            }


            flightPath.setMap(map);
            zoomToObject(flightPath);
        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key= <c:out value="${apiKey}"/>&callback=initMap">
    </script>

</body>
</html>