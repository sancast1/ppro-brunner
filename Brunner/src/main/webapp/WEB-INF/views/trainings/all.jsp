<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="mainTemplate">

    <tiles:putAttribute name="title" value="All Trainings"/>

    <tiles:putAttribute name="body">

        <h1><spring:message code="trainings.h1" /></h1>

        <table class="table table-nobordered"
               id="table"
               data-toggle="table"
               data-unique-id="id"

               data-pagination="true"
               data-page-list="[5, 10, 20, 50, 100, 200]"
               data-search="true"

               data-show-columns="true"
               data-show-pagination-switch="true"
               data-show-export="true"

               data-mobile-responsive="true"
               data-check-on-init="true"

               data-advanced-search="true"
               data-id-table="advancedTable">
        <thead>

            <tr>
                <th data-field="id" data-sortable="true" class="hidden-md hidden-sm hidden-xs">#</th>

                <th data-field="date" data-sortable="true" data-sorter="dateTimeSorter" ><spring:message code="trainings.date" /></th>

                <th data-field="name" data-sortable="true"><spring:message code="trainings.name" /></th>

                <th data-field="distance" data-sortable="true"><spring:message code="trainings.distance" /></th>

                <th data-field="duration" data-sortable="true"><spring:message code="trainings.duration" /></th>

                <th data-field="pace" data-sortable="true"><spring:message code="trainings.pace" /></th>

                <th data-field="type">&nbsp;</th>

                <th data-searchable="false">&nbsp;</th>
                <th data-searchable="false">&nbsp;</th>
                <th data-searchable="false">&nbsp;</th>

            </tr>

        </thead>

        <tbody>

        <c:forEach items="${trainings}" var="training">
            <tr>

                <td>${training.id}</td>

                <td><fmt:formatDate value="${training.date}" pattern="dd.MM.yyyy"/></td>

                <td>${training.name}</td>

                <td>${training.distanceMeters} m</td>

                <td>${training.durationString}</td>

                <td>${training.paceString} min/km</td>

                <td>
                    <c:set var="type" scope="session" value="${training.type}"/>
                    <c:choose>
                        <c:when test="${type =='GPS'}">
                            <a data-placement="top" data-toggle="tooltip" title="Show map"
                               href="<c:url value='/trainings/map/${training.id}' />"
                               class="btn btn-primary btn-xs">
                                <span class="fa fa-globe"></span>
                            </a>
                        </c:when>

                        <c:otherwise>
                            &nbsp;
                        </c:otherwise>
                    </c:choose>
                </td>

                <td>
                    <a data-placement="top" data-toggle="tooltip" title="Detail"
                       class="btn btn-primary btn-xs"
                       href="<c:url value='/trainings/detail/${training.id}' />">
                        <span class="fa fa-line-chart"></span>
                    </a>
                </td>


                <c:choose>
                    <c:when test="${training.type.name() == 'GPS'}">
                        <td>
                            <a data-placement="top" data-toggle="tooltip" title="Edit"
                               class="btn btn-primary btn-xs"
                               href="<c:url value='/trainings/editGPS/${training.id}' />">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>
                    </c:when>

                    <c:otherwise>
                        <td>
                            <a data-placement="top" data-toggle="tooltip" title="Edit"
                               class="btn btn-primary btn-xs"
                               href="<c:url value='/trainings/edit/${training.id}' />">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                        </td>

                    </c:otherwise>
                </c:choose>


                <td>
                    <a data-placement="top" data-toggle="tooltip" title="Delete"
                       class="btn btn-danger btn-xs ajax-delete-user"
                       href="<c:url value='/trainings/delete/${training.id}' />">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>

            </tr>
        </c:forEach>

        </tbody>

        </table>

        <c:if test="${self}">
            <p>
                <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.new.manual" />"
                   class="btn btn-primary btn-xs"
                   href="<c:url value='/trainings/new' />">
                    <span class="fa fa-map fa-2x"></span>&nbsp;
                    <span><spring:message code="trainings.new.manual" /></span> </a>

                <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.new.file" />"
                   class="btn btn-primary btn-xs"
                   href="<c:url value='/trainings/upload' />">
                    <span class="fa fa-globe fa-2x"></span>&nbsp;
                    <span><spring:message code="trainings.new.file" /></span>
                </a>
            </p>
        </c:if>

    </tiles:putAttribute>

    <tiles:putAttribute name="custom-scripts">
        <tag:tooltipInit/>

        <script>
            function dateTimeSorter(a, b) {
                var year1 = a.substring(6, 10) * 1;
                var year2 = b.substring(6, 10) * 1;
                if (year1 > year2) return 1;
                else if (year1 < year2) return -1;
                else {
                    var month1 = a.substring(3, 5) * 1;
                    var month2 = b.substring(3, 5) * 1;
                    if (month1 > month2) return 1;
                    else if (month1 < month2) return -1;
                    else {
                        var day1 = a.substring(0, 2) * 1;
                        var day2 = b.substring(0, 2) * 1;
                        if (day1 > day2) return 1;
                        else if (day1 < day2) return -1;
                        else {
                            return 0;
                        }
                    }
                }
            }
        </script>

    </tiles:putAttribute>

</tiles:insertDefinition>