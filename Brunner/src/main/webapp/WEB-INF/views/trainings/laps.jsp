<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="mainTemplate">

    <tiles:putAttribute name="title" value="Laps of training: ${training.name}" />

    <tiles:putAttribute name="custom-scripts">

        <script type="text/javascript" src="/js/charts-loader.js"></script>

        <!-- draw Lap chart -->
        <script type="text/javascript">

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart500);
            google.charts.setOnLoadCallback(drawChart1000);
            google.charts.setOnLoadCallback(drawChart2000);

            function drawChart500() {
                var data = google.visualization.arrayToDataTable([
                    ['Number', 'Pace'],
                    <c:set var="lapList" value="${allLaps.get(0)}" />

                    <c:forEach items="${lapList.laps}" var="lap">

                    ['${lap.order}', new Date(2000, 1, 1, 0, ${lap.paceMinutes}, ${lap.paceSeconds}, 0)],

                    </c:forEach>
                ]);

                var timeFormatter = new google.visualization.DateFormat({
                    pattern: "m:ss"
                });

                timeFormatter.format(data, 1);
                var options = {
                    title: 'Pace in laps of 500 m',
                    legend: { position: "none" },
                    axisTitlesPosition: 'none',
                    hAxis: { textPosition: 'none' },
                    vAxis: {
                        format: 'm:ss',
                        baselineColor: 'none',
                        ticks: [],
                        textPosition: 'none',
                        scaleType: 'log',
                    },
                    pointSize: 10,
                };
                var chart = new google.visualization.LineChart(document.getElementById('laps500'));
                chart.draw(data, {vAxis: {direction: -1}}, options);
            }

            function drawChart1000() {
                var data = google.visualization.arrayToDataTable([
                    ['Number', 'Pace'],
                    <c:set var="lapList" value="${allLaps.get(1)}" />

                    <c:forEach items="${lapList.laps}" var="lap">

                    ['${lap.order}', new Date(2000, 1, 1, 0, ${lap.paceMinutes}, ${lap.paceSeconds}, 0)],

                    </c:forEach>
                ]);

                var timeFormatter = new google.visualization.DateFormat({
                    pattern: "m:ss"
                });

                timeFormatter.format(data, 1);
                var options = {
                    title: 'Pace in laps of 1 km',
                    legend: { position: "none" },
                    axisTitlesPosition: 'none',
                    hAxis: { textPosition: 'none' },
                    vAxis: {
                        format: 'm:ss',
                        baselineColor: 'none',
                        ticks: [],
                        textPosition: 'none',
                        scaleType: 'log',
                    },
                    pointSize: 10,
                };
                var chart = new google.visualization.LineChart(document.getElementById('laps1000'));
                chart.draw(data, {vAxis: {direction: -1}}, options);
            }

            function drawChart2000() {
                var data = google.visualization.arrayToDataTable([
                    ['Number', 'Pace'],
                    <c:set var="lapList" value="${allLaps.get(2)}" />

                    <c:forEach items="${lapList.laps}" var="lap">

                    ['${lap.order}', new Date(2000, 1, 1, 0, ${lap.paceMinutes}, ${lap.paceSeconds}, 0)],

                    </c:forEach>
                ]);

                var timeFormatter = new google.visualization.DateFormat({
                    pattern: "m:ss"
                });

                timeFormatter.format(data, 1);
                var options = {
                    title: 'Pace in laps of 2000 m',
                    legend: { position: "none" },
                    axisTitlesPosition: 'none',
                    hAxis: { textPosition: 'none' },
                    vAxis: {
                        format: 'm:ss',
                        baselineColor: 'none',
                        ticks: [],
                        textPosition: 'none',
                        scaleType: 'log',
                    },
                    pointSize: 10,
                };
                var chart = new google.visualization.LineChart(document.getElementById('laps2000'));
                chart.draw(data, {vAxis: {direction: -1}}, options);
            }


            google.load("visualization", "1", { packages: ["corechart"] });
            google.setOnLoadCallback(drawChart);

        </script>


    </tiles:putAttribute>

    <tiles:putAttribute name="body">



        <h1><spring:message code="trainings.laps" />: ${training.name} (<fmt:formatDate value="${training.date}" pattern="dd.MM.yyyy"/>)</h1>


        <h2><spring:message code="trainings.fiveM" /></h2>

        <div id="laps500" style="width: 85%; height: 220px; margin: auto;" class="row">

            <c:set var="lapList" value="${allLaps.get(0)}" />

            <c:forEach items="${lapList.laps}" var="lap">

                ${lap.toString()}

                <br>

            </c:forEach>

        </div>

        <div class="clear-both"></div>

            <br>

        <h2><spring:message code="trainings.tenM" /></h2>

        <div id="laps1000" style="width: 85%; height: 220px; margin: auto;" class="row">

            <c:set var="lapList" value="${allLaps.get(1)}" />

            <c:forEach items="${lapList.laps}" var="lap">

                ${lap.toString()}

            </c:forEach>

        </div>

        <div class="clear-both"></div>

            <br>

        <h2><spring:message code="trainings.tventyM" /></h2>

        <div id="laps2000" style="width: 85%; height: 220px; margin: auto;" class="row">

            <c:set var="lapList" value="${allLaps.get(2)}" />

            <c:forEach items="${lapList.laps}" var="lap">

                ${lap.toString()}

            </c:forEach>

        </div>

        <div class="clear-both"></div>

            <br>

    </tiles:putAttribute>

</tiles:insertDefinition>