<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="mainTemplate">

    <tiles:putAttribute name="header">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    </tiles:putAttribute>

    <tiles:putAttribute name="title" value="Training: ${training.name}" />

    <tiles:putAttribute name="body">

        <h1>${training.name} (<fmt:formatDate value="${training.date}" pattern="dd.MM.yyyy"/>)</h1>

            <div class="row">

                <div class="container-general-info col-md-6 col-xs-12">

                    <h2><spring:message code="trainings.general" /></h2>

                    <c:if test="${training.description != ''}">
                        <span><b><spring:message code="trainings.desc" />:</b></span>
                        <p>${training.description}</p>
                        <br>
                    </c:if>


                    <table class="table table-sm">
                        <tr>
                            <td><span class="fa fa-road"></span>&nbsp;</td>
                            <td><spring:message code="trainings.distance" /></td>
                            <td><b>${training.distanceMeters} m</b></td>
                        </tr>

                        <tr>
                            <td><span class="fa fa-clock-o"></span></td>
                            <td><spring:message code="trainings.duration" /></td>
                            <td><b>${training.durationString}</b></td>
                        </tr>

                        <tr>
                            <td><span class="fa fa-line-chart"></span></td>
                            <td><spring:message code="trainings.pace" /></td>
                            <td><b>${training.paceString} min/km</b></td>
                        </tr>

                        <tr>
                            <td><span class="fa fa-arrow-up"></span></td>
                            <td><spring:message code="trainings.distance.asc" /></td>
                            <td><b>${training.distanceMetersAscent} m</b></td>
                        </tr>

                        <tr>
                            <td><span class="fa fa-arrow-down"></span></td>
                            <td><spring:message code="trainings.distance.desc" /></td>
                            <td><b>${training.distanceMetersDescent} m</b></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><spring:message code="trainings.elevation.gain" /></td>
                            <td><b>${training.elevationGained} m</b></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><spring:message code="trainings.elevation.asc" /></td>
                            <td><b>${training.elevationAscent} m</b></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><spring:message code="trainings.elevation.desc" /></td>
                            <td><b>${training.elevationDescent} m</b></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><spring:message code="trainings.elevation.max" /></td>
                            <td><b>${training.elevationMax} m</b></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><spring:message code="trainings.elevation.min" /></td>
                            <td><b>${training.elevationMin} m</b></td>
                        </tr>

                        <tr>
                            <td><span class="fa fa-heartbeat"></span></td>
                            <td><spring:message code="trainings.heart.avg" /></td>
                            <td><b>${training.heartRateAvg} bpm</b></td>
                        </tr>

                        <tr>
                            <td><span class="fa fa-heart"></span></td>
                            <td><spring:message code="trainings.heart.max" />&nbsp;</td>
                            <td><b>${training.heartRateMax} bpm</b></td>
                        </tr>

                    </table>

                </div>

            </div>

        </br>
        </br>

        <div class="row">

            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.delete" />"
               class="btn btn-danger btn-lg"
               href="<c:url value='/trainings/delete/${training.id}' />">
                <span class="fa fa-trash"></span>
                <span><spring:message code="trainings.delete.button" /></span> </a>

            &nbsp;

            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.delete" />"
               class="btn btn-warning btn-lg"
               href="<c:url value='/trainings/edit/${training.id}' />">
                <span class="fa fa-pencil"></span>
                <span><spring:message code="trainings.edit" /></span> </a>

            &nbsp;

            <c:choose>
                <c:when test="${self}">
                    <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                       class="btn btn-primary btn-lg"
                       href="<c:url value='/trainings/' />">
                        <span class="fa fa-heartbeat"></span>
                        <span><spring:message code="trainings.back" /></span> </a>
                </c:when>

                <c:otherwise>
                    <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                       class="btn btn-primary btn-lg"
                       href="<c:url value='/trainings/user/${ownerId}' />">
                        <span class="fa fa-heartbeat"></span>
                        <span><spring:message code="trainings.back" /></span> </a>
                </c:otherwise>

            </c:choose>

        </div>

    </tiles:putAttribute>

</tiles:insertDefinition>