<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="New training" />
<tiles:putAttribute name="body">

	<h1><spring:message code="trainings.new" /></h1>

    <form:form method="POST" modelAttribute="training">

        <div class="row" style="margin: 15px;">

            <div class="col-md-7 col-xs-12">

                <spring:message code="trainings.name" var="name" />
                <tag:inputField name="name" label="${name}" type="text"/>

                <spring:message code="trainings.date" var="date" />
                <tag:inputField name="date" label="${date}" type="date"/>

                <label><spring:message code="trainings.duration" />: </label>
                <div class="row">
                    <div class="col-sm-2">
                        <label><spring:message code="trainings.hours" />: </label>
                        <form:input path="durationHours" id="durationHours" class="form-control col-x-xx duration"
                                    type="number"  min="0" max="23" onkeypress='return numbersOnly(event);' />
                    </div>

                    <div class="col-sm-2">
                        <label><spring:message code="trainings.minutes" />: </label>
                        <form:input path="durationMinutes" id="durationMinutes" class="form-control col-x-xx duration"
                                    type="number"  min="0" max="59" onkeypress='return numbersOnly(event);' />
                    </div>

                    <div class="col-sm-2">
                        <label><spring:message code="trainings.seconds" />: </label>
                        <form:input path="durationSeconds" id="durationSeconds" class="form-control col-x-xx duration"
                                    type="number"  min="0" max="59" onkeypress='return numbersOnly(event);' />
                    </div>
                </div>

                </br>

                <spring:message code="trainings.distance" var="distance" />
                <tag:inputField name="distanceMeters" label="${distance} (m)" type="number" min="1"/>

                <spring:message code="trainings.desc" var="desc" />
                <tag:inputField name="description" label="${desc}" type="textarea"/>

                <spring:message code="trainings.heart.avg" var="heartAvg" />
                <tag:inputField name="heartRateAvg" label="${heartAvg} (bpm)" type="number" min="0"/>

                <!-- show more tag -->

                <spring:message code="trainings.heart.max" var="heartMax" />
                <tag:inputField name="heartRateMax" label="${heartMax} (bpm)" type="number" min="0" max="230"/>

                <spring:message code="trainings.distance.asc" var="distanceAsc" />
                <tag:inputField name="distanceMetersAscent" label="${distanceAsc} (m)" type="number" min="0"/>

                <spring:message code="trainings.distance.desc" var="distanceDesc" />
                <tag:inputField name="distanceMetersDescent" label="${distanceDesc} (m)" type="number" min="0"/>

                <spring:message code="trainings.elevation.gain" var="elevationGain" />
                <tag:inputField name="elevationGained" label="${elevationGain} (m)" type="number" />

                <spring:message code="trainings.elevation.asc" var="elevationAsc" />
                <tag:inputField name="elevationAscent" label="${elevationAsc} (m)" type="number" min="0"/>

                <spring:message code="trainings.elevation.desc" var="elevationDesc" />
                <tag:inputField name="elevationDescent" label="${elevationDesc} (m)" type="number" min="0"/>

                <spring:message code="trainings.elevation.min" var="elevationMin" />
                <tag:inputField name="elevationMin" label="${elevationMin} (m)" type="number" min="-12000" max="8848"/>

                <spring:message code="trainings.elevation.max" var="elevationMax" />
                <tag:inputField name="elevationMax" label="${elevationMax} (m)" type="number" min="-12000" max="8848"/>

                <!-- / show more tag -->

                <br/>

                <button type="submit" class="btn btn-success btn-lg">
                    <spring:message code="trainings.save" /></button>

                &nbsp;

                <a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
                   class="btn btn-primary btn-lg"
                   href="<c:url value='/trainings/' />">
                    <span class="fa fa-heartbeat"></span>
                    <span><spring:message code="trainings.back" /></span> </a>

            </div>

        </div>
    </form:form>

	<br/>

</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
    <tag:tooltipInit/>
    <script>
        function numbersOnly(event) {
            var key = (event.hasOwnProperty('charCode')) ? event.charCode : event.which;
            // Return "true" for all numbers and the delete (a.k.a., backspace) key.
            return ((key >= 48 && key <= 57) || key == 8) ? true : false;
        }
    </script>
</tiles:putAttribute>

</tiles:insertDefinition>