<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Delete training: ${training.name}" />
<tiles:putAttribute name="body">

	<h1><spring:message code="trainings.delete" /> <span>${training.name}</span></h1>

	<span><spring:message code="trainings.delete.conf" /> <i>${training.name}</i>?</span>

		<br/>
		<br/>

	<form:form method="POST">

		<button type="submit" class="btn btn-danger btn-lg">
			<span class="fa fa-trash"></span>
		<spring:message code="trainings.delete.button" /></button>

		&nbsp;

		<c:choose>
			<c:when test="${self}">
				<a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
				   class="btn btn-primary btn-lg"
				   href="<c:url value='/trainings/' />">
					<span class="fa fa-heartbeat"></span>
					<span><spring:message code="trainings.back" /></span> </a>
			</c:when>

			<c:otherwise>
				<a data-placement="top" data-toggle="tooltip" title="<spring:message code="trainings.back" />"
				   class="btn btn-primary btn-lg"
				   href="<c:url value='/trainings/user/${training.user.id}' />">
					<span class="fa fa-heartbeat"></span>
					<span><spring:message code="trainings.back" /></span> </a>
			</c:otherwise>

		</c:choose>

	</form:form>
	<br/>

</tiles:putAttribute>

</tiles:insertDefinition>