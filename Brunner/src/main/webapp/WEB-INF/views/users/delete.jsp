<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Delete user: ${user.login}" />
<tiles:putAttribute name="body">

	<h1><spring:message code="user.deleteUser" />
		<span>${user.firstName}</span>
		<span>${user.lastName}</span>
	</h1>

	<span>
		<spring:message code="user.deleteUser.Confrim" /> <i>${user.firstName} ${user.lastName}</i>?</span>
	</span>

		<br/>
		<br/>

	<form:form method="POST">

		<button type="submit" class="btn btn-danger btn-lg">
			<span class="fa fa-trash"></span>
			<spring:message code="user.delete" /></button>

			&nbsp;

		<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.new" />"
		   class="btn btn-primary btn-lg"
		   href="<c:url value='/users/' />">
			<span class="fa fa-users"></span>
			<span><spring:message code="user.all" /></span> </a>

	</form:form>
	<br/>

</tiles:putAttribute>

</tiles:insertDefinition>