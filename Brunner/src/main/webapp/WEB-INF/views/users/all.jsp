<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="All users" />

<tiles:putAttribute name="body">

	<h1><spring:message code="user.all" /></h1>


	<table class="table table-nobordered"
		   id="table"
		   data-toggle="table"
		   data-unique-id="id"

		   data-pagination="true"
		   data-page-list="[5, 10, 20, 50, 100, 200]"
		   data-search="true"

		   data-show-columns="true"
		   data-show-pagination-switch="true"
		   data-show-export="true"

		   data-mobile-responsive="true"
		   data-check-on-init="true"

		   data-advanced-search="true"
		   data-id-table="advancedTable">


		<thead>

			<tr>
				<th data-field="id" data-sortable="true" class="hidden-md hidden-sm hidden-xs">#</th>

				<th data-field="login" data-sortable="true"><spring:message code="user.login" /></th>

				<th data-field="firstName | lastName" data-sortable="true"><spring:message code="user.name" /></th>

				<th data-field="birthDate" data-sortable="true" data-sorter="dateTimeSorter" class="hidden-md hidden-sm hidden-xs">
					<spring:message code="user.birthdate" />
				</th>

				<th data-field="role" data-sortable="true" class="hidden-xs"><spring:message code="user.role" /></th>

				<th data-field="enabled" data-sortable="true" class="hidden-xs"><spring:message code="user.enabled" /></th>

				<th data-searchable="false">&nbsp;</th>
				<th data-searchable="false">&nbsp;</th>
				<th data-searchable="false">&nbsp;</th>
				<th data-searchable="false">&nbsp;</th>
				<th data-searchable="false">&nbsp;</th>
			</tr>

		</thead>


		<tbody>

			<c:forEach items="${users}" var="user">
				<tr>
					<td>${user.id}</td>

					<td>${user.login}</td>

					<td>
						<span>${user.lastName}</span>
						<span>${user.firstName}</span>
						<span>
						<c:set var="gender" scope="session" value="${user.gender}"/>
						<c:choose>
							<c:when test="${gender =='MAN'}">
								<span class="fa fa-mars"></span>
							</c:when>

							<c:otherwise>
								<span class="fa fa-venus"></span>
							</c:otherwise>
						</c:choose>
						</span>
					</td>

					<td><fmt:formatDate value="${user.birthDate}" pattern="dd.MM.yyyy"/></td>

					<td>${user.role.value}</td>

                    <td>${user.enabled}</td>

					<td>
						<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.detail" />"
						   class="btn btn-primary btn-xs"
						   href="<c:url value='/profile/detail/${user.id}' />">
							<span class="fa fa-user"></span>
						</a>
					</td>

					<td>
						<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.trainings" />"
							class="btn btn-primary btn-xs"
							href="<c:url value='/trainings/user/${user.id}' />">
							<span class="fa fa-heartbeat"></span>
						</a>
					</td>

					<td>
						<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.edit" />"
						   class="btn btn-primary btn-xs"
						   href="<c:url value='/profile/edit/${user.id}' />">
							<span class="fa fa-pencil"></span>
						</a>
					</td>

					<td>
						<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.edit.passwd" />"
						   class="btn btn-primary btn-xs"
						   href="<c:url value='/profile/edit-password/${user.id}' />">
							<span class="fa fa-key"></span>
						</a>
					</td>

					<td>
						<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.delete" />"
						   class="btn btn-danger btn-xs"
						   href="<c:url value='/users/delete/${user.id}' />">
							<span class="fa fa-trash"></span>
						</a>
					</td>
				</tr>
			</c:forEach>

		</tbody>

	</table>

	<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.new" />"
	   class="btn btn-primary btn-xs"
	   href="<c:url value='/users/new' />">
		<span class="fa fa-user-plus fa-2x"></span>
		<span><spring:message code="user.new" /></span>
	</a>

</tiles:putAttribute>

<tiles:putAttribute name="custom-scripts">
	<tag:tooltipInit/>

	<script>
		function dateTimeSorter(a, b) {
			var year1 = a.substring(6, 10) * 1;
			var year2 = b.substring(6, 10) * 1;
			if (year1 > year2) return 1;
			else if (year1 < year2) return -1;
			else {
				var month1 = a.substring(3, 5) * 1;
				var month2 = b.substring(3, 5) * 1;
				if (month1 > month2) return 1;
				else if (month1 < month2) return -1;
				else {
					var day1 = a.substring(0, 2) * 1;
					var day2 = b.substring(0, 2) * 1;
					if (day1 > day2) return 1;
					else if (day1 < day2) return -1;
					else {
						return 0;
					}
				}
			}
		}
	</script>

</tiles:putAttribute>

</tiles:insertDefinition>