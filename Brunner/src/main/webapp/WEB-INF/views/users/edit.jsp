<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="mainTemplate">
	<tiles:putAttribute name="title" value="Edit user: ${user.login}" />
	<tiles:putAttribute name="body">

		<h1><spring:message code="user.h2edit" />
            <span>${user.firstName}</span>
            <span>${user.lastName}</span>
        </h1>

        <form:form method="POST" modelAttribute="user">
            <div class="row" style="margin: 15px;">
                <div class="col-md-7 col-xs-12">

                    <sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
                        <spring:message code="user.login" var="login" />
                        <tag:inputField name="login" label="${login}" type="text"/>
                    </sec:authorize>

                    <spring:message code="user.first" var="first" />
                    <tag:inputField name="firstName" label="${first}" type="text"/>

                    <spring:message code="user.last" var="last" />
                    <tag:inputField name="lastName" label="${last}" type="text"/>

                    <spring:message code="user.birthdate" var="birth" />
                    <tag:inputField name="birthDate" label="${birth}" type="date"/>

                    <spring:message code="user.gender" var="gender" />
                    <tag:inputField name="gender" label="${gender}" type="select"/>

                    <spring:message code="user.height" var="height" />
                    <tag:inputField name="height" label="${height}" type="text"/>

                    <spring:message code="user.weight" var="weight" />
                    <tag:inputField name="weight" label="${weight}" type="text"/>

                    <sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
                        <spring:message code="user.role" var="role" />
                        <tag:inputField name="role" label="${role}" type="select" disabled="${self}" />

                        <div class="checkbox">
                            <label>
                                <form:checkbox path="enabled" id="enabled" disabled="${self}"/>
                                <b><spring:message code="user.enabled" /></b>
                            </label>
                        </div>
                    </sec:authorize>

                    <button type="submit" class="btn btn-success btn-lg"><spring:message code="user.update" /></button>
                        &nbsp;
                    <sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
                        <a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.all" />"
                           class="btn btn-primary btn-lg"
                           href="<c:url value='/users/' />">
                            <span class="fa fa-users"></span>
                            <span><spring:message code="user.all" /></span> </a>
                    </sec:authorize>

                    <sec:authorize access="hasRole('ROLE_USER') and isAuthenticated()">
                        <a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.detail" />"
                           class="btn btn-primary btn-lg"
                           href="<c:url value='/profile/detail/${user.id}' />">
                            <span class="fa fa-user"></span>
                            <span><spring:message code="user.detail" /></span> </a>
                    </sec:authorize>
                </div>
            </div>
        </form:form>

		<br/>

	</tiles:putAttribute>

</tiles:insertDefinition>