<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="Edit password: ${user.login}" />
<tiles:putAttribute name="body">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
	<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<h1><spring:message code="user.edit.passwd.h1" />
		<span>${user.firstName}</span>
		<span>${user.lastName}</span>
	</h1>

	<form:form method="POST" modelAttribute="user">
		<div class="row" style="margin: 15px;">
			<div class="col-md-7 col-xs-12">

				<spring:message code="user.passwd" var="passwd" />
				<tag:inputField name="password" label="${passwd}" type="password"/>

				<spring:message code="user.edit.passwd.confrim" var="passwd2" />
				<tag:inputField name="confirmPassword" label="${passwd2}" type="password"/>

				<button type="submit" class="btn btn-success btn-lg"><spring:message code="user.update" /></button>
				&nbsp;
				<sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
					<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.all" />"
					   class="btn btn-primary btn-lg"
					   href="<c:url value='/users/' />">
						<span class="fa fa-users"></span>
						<span><spring:message code="user.all" /></span> </a>
				</sec:authorize>

				<sec:authorize access="hasRole('ROLE_USER') and isAuthenticated()">
					<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.detail" />"
					   class="btn btn-primary btn-lg"
					   href="<c:url value='/profile/detail/${user.id}' />">
						<span class="fa fa-user"></span>
						<span><spring:message code="user.detail" /></span> </a>
				</sec:authorize>
			</div>
		</div>
	</form:form>

	<br/>

</tiles:putAttribute>

</tiles:insertDefinition>