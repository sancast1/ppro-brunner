<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<tiles:insertDefinition name="mainTemplate">
    <tiles:putAttribute name="title" value="User: ${user.login}" />
    <tiles:putAttribute name="body">
        <div class="detail">
            <div class="row">

                <h1>
                    <span>${user.firstName}</span>
                    <span>${user.lastName} </span>
                    <span>
                    <c:set var="gender" scope="session" value="${user.gender}"/>
                    <c:choose>
                        <c:when test="${gender =='MAN'}">
                                <span class="fa fa-mars"></span>
                        </c:when>

                        <c:otherwise>
                                <span class="fa fa-venus"></span>
                        </c:otherwise>
                    </c:choose>
                    </span>
                </h1>

                <div class="col-sm-7 col-xs-12">
                    <p>
                        <a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.edit" />"
                       class="btn btn-primary btn-xs"
                       href="<c:url value='/profile/edit/${user.id}' />">
                        <span class="fa fa-pencil"></span>
                        <span><spring:message code="user.edit" /></span> </a>

                        <a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.edit.passwd" />"
                           class="btn btn-primary btn-xs"
                           href="<c:url value='/profile/edit-password/${user.id}' />">
                            <span class="fa fa-key"></span>
                            <span><spring:message code="user.edit.passwd" /></span></a>

                        <sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
                            <a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.delete" />"
                            class="btn btn-danger btn-xs ajax-delete-user"
                            href="<c:url value='/users/delete/${user.id}' />">
                            <span class="fa fa-trash"></span>
                            <span><spring:message code="user.delete" /></span></a>
                        </sec:authorize>
                    </p>

                    <table class="table table-hover">
                        <tr><td><spring:message code="user.login" /></td><td>${user.login}</td></tr>

                        <tr>
                            <td><spring:message code="user.name" /></td><td>
                              <span>${user.firstName}</span>
                              <span>${user.lastName}</span>
                            </td>
                        </tr>

                        <tr>
                            <td><spring:message code="user.birthdate" /></td>

                            <td><fmt:formatDate value="${user.birthDate}" pattern="dd.MM.yyyy"/></td>
                        </tr>

                        <tr><td><spring:message code="user.age" /></td><td>${user.age}</td></tr>

                        <tr><td><spring:message code="user.height" /></td><td>${user.height} cm</td></tr>

                        <tr><td><spring:message code="user.weight" /></td><td>${user.weight} kg</td></tr>

                        <tr><td>BMI</td>
                            <td><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="2" value="${user.BMI}" /></td>
                        </tr>

                        <sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
                            <tr><td><spring:message code="user.role" /></td><td>${user.role}</td></tr>

                            <tr><td><spring:message code="user.enabled" /></td><td>${user.enabled}</td></tr>
                        </sec:authorize>

                    </table>

                    <sec:authorize access="hasRole('ROLE_ADMIN') and isAuthenticated()">
                        <a data-placement="top" data-toggle="tooltip"
                           title="<spring:message code="user.all" />"
                           class="btn btn-primary btn-sm"
                           href="<c:url value='/users/' />">
                            <span class="fa fa-users"></span>
                            <span><spring:message code="user.all" /></span> </a>
                    </sec:authorize>

                </div>
            </div>
        </div>

    </tiles:putAttribute>

</tiles:insertDefinition>