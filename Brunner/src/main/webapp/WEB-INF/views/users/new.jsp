<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="mainTemplate">
<tiles:putAttribute name="title" value="New user" />
<tiles:putAttribute name="body">

	<h1><spring:message code="user.new" /></h1>

	<form:form method="POST" modelAttribute="user">
		<div class="row" style="margin: 15px;">
			<div class="col-md-7 col-xs-12">

				<spring:message code="user.first" var="first" />
				<tag:inputField name="firstName" label="${first}" type="text"/>

				<spring:message code="user.last" var="last" />
				<tag:inputField name="lastName" label="${last}" type="text"/>

				<spring:message code="user.birthdate" var="birthdate" />
				<tag:inputField name="birthDate" label="${birthdate}" type="date"/>

				<spring:message code="user.gender" var="gender" />
				<tag:inputField name="gender" label="${gender}" type="select"/>

				<spring:message code="user.height" var="height" />
				<tag:inputField name="height" label="${height} (cm)" type="number" min="50" max="230" value="175"/>

				<spring:message code="user.weight" var="weight" />
				<tag:inputField name="weight" label="${weight} (kg)" type="number" min="50" max="300" value="70"/>

				<spring:message code="user.login" var="login" />
				<tag:inputField name="login" label="${login}" type="text"/>

				<spring:message code="user.new.passwd" var="passwd" />
				<tag:inputField name="password" label="${passwd}" type="password"/>

				<spring:message code="user.new.passwd.confrim" var="passwdc" />
				<tag:inputField name="confirmPassword" label="${passwdc}" type="password"/>

				<spring:message code="user.role" var="role" />
                <tag:inputField name="role" label="${role}" type="select"/>

                <div class="checkbox">
					<label><form:checkbox path="enabled" id="enabled" />
						<b><spring:message code="user.enabled" /></b>
					</label>
				</div>

				<button type="submit" class="btn btn-success btn-lg"><spring:message code="user.regiser" /></button>

				&nbsp;

				<a data-placement="top" data-toggle="tooltip" title="<spring:message code="user.all" />"
				   class="btn btn-primary btn-lg"
				   href="<c:url value='/users/' />">
					<span class="fa fa-users"></span>
					<span><spring:message code="user.all" /></span> </a>
			</div>
		</div>
	</form:form>
	<br/>

</tiles:putAttribute>

</tiles:insertDefinition>