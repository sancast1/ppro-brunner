<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="loginTemplate">
	<spring:message var="title" code="login.title" />
    <tiles:putAttribute name="title" value="Brunner | ${title}" />
    <tiles:putAttribute name="body">

        <div class="center-block row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

                <h1><spring:message code="login.h1" /></h1>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger">${error}</div>
                </c:if>

                <c:if test="${not empty msg}">
                    <div class="alert alert-success">${msg}</div>
                </c:if>

                <form name="loginForm" class="form-horizontal"
                      action="<c:url value='/login' />" method='POST'>

                    <div class="form-group">
                    	<spring:message var="label" code="login.login" />
                        <label for="login" class="control-label col-xs-2">${label}</label>
                        <div class="col-xs-10">
                            <input type="text" name="login" class="form-control" placeholder="${label}">
                        </div>
                    </div>

                    <div class="form-group">
                    	<spring:message var="label" code="login.password" />
                        <label for="password" class="control-label col-xs-2">${label}</label>
                        <div class="col-xs-10">
                            <input type="password" name="password" class="form-control" placeholder="${label}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-offset-2 col-xs-10">
                            <button type="submit" class="btn btn-primary btn-lg"><spring:message code="login.button" /></button>
                        </div>
                    </div>

                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

                </form>
            </div>
        </div>

    </tiles:putAttribute>

</tiles:insertDefinition>