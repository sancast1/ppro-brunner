<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="mainTemplate">
	<spring:message var="denied_title" code="denied.title" />
	<tiles:putAttribute name="title" value="${denied_title}" />
	<tiles:putAttribute name="body">

	<div class="text-center">
		<h1><spring:message code="denied.h1" /></h1>
		<h2><spring:message code="denied.h2" /></h2>
	</div>

    </tiles:putAttribute>

</tiles:insertDefinition>